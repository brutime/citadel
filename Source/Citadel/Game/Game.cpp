//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Game.h"

#include "../Common/EntityManager.h"
#include "../Common/UpdatingOrderTable.h"
#include "../MainMenu/MainMenu.h"
#include "../Tutorial/Tutorial.h"
#include "../Gameplay/NewGame.h"
#include "../Gameplay/LocationComplete.h"
#include "../Gameplay/RestartLocation.h"
#include "../Gameplay/NextLocation.h"
#include "../Gameplay/EndGame.h"
#include "../Gameplay/GameOver.h"
#include "../Outro/Outro.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

SharedPtr<GameSingleton> Game;

//===========================================================================//

const string GameSingleton::Name = "Citadel";
const string GameSingleton::Version = "0.1.1.2 alpha";

const string GameSingleton::SharedDataFileName = "SharedData";

const string GameSingleton::PathToTextures = "Textures";
const string GameSingleton::PathToPathToTexture = "PathToTexture";

const string GameSingleton::PathToParametersFiles = "ParametersFiles";
const string GameSingleton::PathToPathParameterFiles = "PathToParametersFile";

//===========================================================================//

GameSingleton::GameSingleton():
    _states(),
    _currentGameState(GameState::None),
    _statesQueue()
{
    Console->Notice(string::Format("Game \"{0}\" initializing...", Name));
    Console->AddOffset(2);

    Console->Notice(string::Format("Game version: \"{0}\"", Version));

    LoadSharedData();

    UpdatingOrderTable = new UpdatingOrderTableSingleton();
    EntityManager = new EntityManagerSingleton();

    _states.Resize(static_cast<integer>(GameState::Count));
    _states[static_cast<integer>(GameState::Intro)] = new Intro();
    _states[static_cast<integer>(GameState::MainMenu)] = new MainMenu();
    _states[static_cast<integer>(GameState::Tutorial)] = new Tutorial();
    _states[static_cast<integer>(GameState::NewGame)] = new NewGame();
    _states[static_cast<integer>(GameState::LoadLocation)] = new LoadLocation();
    _states[static_cast<integer>(GameState::Gameplay)] = new Gameplay();
    _states[static_cast<integer>(GameState::RestartLocation)] = new RestartLocation();
    _states[static_cast<integer>(GameState::LocationComplete)] = new LocationComplete();
    _states[static_cast<integer>(GameState::NextLocation)] = new NextLocation();
    _states[static_cast<integer>(GameState::GameOver)] = new GameOver();
    _states[static_cast<integer>(GameState::EndGame)] = new EndGame();
    _states[static_cast<integer>(GameState::Outro)] = new Outro();

    AddStateToQueue(GameState::Intro);

    Console->ReduceOffset(2);
    Console->Notice("Game initialized");
}

//===========================================================================//

GameSingleton::~GameSingleton()
{
    Console->Notice("Game shutting down...");
    Console->AddOffset(2);

    for (integer i = static_cast<integer>(GameState::None) + 1; i < static_cast<integer>(GameState::Count); i++)
    {
        _states[i] = NullPtr;
    }
    _states.Clear();

    EntityManager = NullPtr;
    UpdatingOrderTable = NullPtr;

    Console->ReduceOffset(2);
    Console->Notice("Game has shut down");
}

//===========================================================================//

void GameSingleton::AddStateToQueue(GameState newGameState)
{
    if (_statesQueue.IsEmpty() || _statesQueue.GetLast() != newGameState)
    {
        _statesQueue.Add(newGameState);
    }
}

//===========================================================================//

void GameSingleton::UpdateStatesQueue()
{
    for (GameState newGameState : _statesQueue)
    {
        integer currentGameStateIndex = static_cast<integer>(_currentGameState);
        integer newGameStateIndex = static_cast<integer>(newGameState);

        if (_currentGameState != GameState::None)
        {
            _states[currentGameStateIndex]->Leave();
        }

        if (newGameState != GameState::None)
        {
            _states[newGameStateIndex]->Select();
        }

        _currentGameState = newGameState;
    }

    if (!_statesQueue.IsEmpty())
    {
        _statesQueue.Clear();
        Engine->ResetTiming();
    }
}

//===========================================================================//

SharedPtr<Intro> GameSingleton::GetIntro() const
{
    integer introIndex = static_cast<integer>(GameState::Intro);
    return static_pointer_cast<Intro>(_states[introIndex]);
}

//===========================================================================//

SharedPtr<LoadLocation> GameSingleton::GetLoadLocation() const
{
    integer loadLocationIndex = static_cast<integer>(GameState::LoadLocation);
    return static_pointer_cast<LoadLocation>(_states[loadLocationIndex]);
}

//===========================================================================//

SharedPtr<Gameplay> GameSingleton::GetGameplay() const
{
    integer gameplayIndex = static_cast<integer>(GameState::Gameplay);
    return static_pointer_cast<Gameplay>(_states[gameplayIndex]);
}

//===========================================================================//

void GameSingleton::LoadSharedData()
{
    WeakPtr<ParametersFile> sharedDataFile = ParametersFileManager->Load(SharedDataFileName);

    integer texturesCount = sharedDataFile->GetListChildren(PathToTextures);
    for (integer i = 0; i < texturesCount; i++)
    {
        string parameterPath = string::Format(PathToTextures + ".{0}." + PathToPathToTexture, i);
        string pathToTexture = sharedDataFile->GetString(parameterPath);
        TextureManager->Load(pathToTexture);
    }

    integer parametersFilesCount = sharedDataFile->GetListChildren(PathToParametersFiles);
    for (integer i = 0; i < parametersFilesCount; i++)
    {
        string parameterPath = string::Format(PathToParametersFiles + ".{0}." + PathToPathParameterFiles, i);
        string pathToParametersFile = sharedDataFile->GetString(parameterPath);
        ParametersFileManager->Load(pathToParametersFile);
    }

    FontManager->Load("Edisson", 40);
}

//===========================================================================//

} // namespace Citadel

