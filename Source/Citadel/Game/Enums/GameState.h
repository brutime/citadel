//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    ��������:
//

#pragma once
#ifndef GAME_STATE_H
#define GAME_STATE_H

namespace Citadel
{

//===========================================================================//

enum class GameState
{
    None,
    Intro,
    MainMenu,
    Tutorial,
    NewGame,
    LoadLocation,
    Gameplay,
    RestartLocation,
    LocationComplete,
    NextLocation,
    GameOver,
    EndGame,
    Outro,
    Count
};

//===========================================================================//

} // namespace Citadel

#endif // GAME_STATE_H
