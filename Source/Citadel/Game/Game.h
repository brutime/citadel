//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef GAME_H
#define GAME_H

#include "../../BruTech/Source/Helpers/Engine/Engine.h"

#include "Enums/GameState.h"
#include "IGameState.h"
#include "../Intro/Intro.h"
#include "../Gameplay/Gameplay.h"
#include "../Gameplay/LoadLocation.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class GameSingleton
{
public:
    static const string Name;
    static const string Version;

    GameSingleton();
    ~GameSingleton();

    void AddStateToQueue(GameState newGameState);
    void UpdateStatesQueue();

    SharedPtr<Intro> GetIntro() const;
    SharedPtr<LoadLocation> GetLoadLocation() const;
    SharedPtr<Gameplay> GetGameplay() const;

private:
    static const string SharedDataFileName;

    static const string PathToTextures;
    static const string PathToPathToTexture;

    static const string PathToParametersFiles;
    static const string PathToPathParameterFiles;

    void LoadSharedData();

    Array<SharedPtr<IGameState>> _states;
    GameState _currentGameState;

    List<GameState> _statesQueue;

    GameSingleton(const GameSingleton &) = delete;
    GameSingleton & operator =(const GameSingleton &) = delete;
};

//===========================================================================//

extern SharedPtr<GameSingleton> Game;

//===========================================================================//

} // namespace Citadel

#endif // GAME_H
