//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Outro.h"

#include "../Game/Game.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

const string Outro::Name = "Outro state";
const string Outro::DomainName = "Outro";
const string Outro::LayerName = "Outro";

//===========================================================================//

Outro::Outro(): IGameState(),
    _frame(),
    _goToMainMenuButton(),
    _inputReceiver(),
    _camera(),
    _goToMainMenuButtonClickHandler(new EventHandler<const MouseMessage &>(this, &Outro::OnGoToMainMenuButtonClick))
{
    Console->Notice(string::Format("{0} initializing...", Name));
    Console->AddOffset(2);

    DomainManager->Add(DomainName);

    Renderer->AddLayerToFront(DomainName, new RenderableLayer(LayerName, ProjectionMode::Ortho));

    _frame = new GUIFrame(DomainName, LayerName, 1.0f, 1.0f, "Widgets/OutroBackground");

    _goToMainMenuButton = new Button(DomainName, LayerName, 0.3418f, 0.1771f, "Widgets/ButtonQuitToMenuNormal", "Widgets/ButtonQuitToMenuPressed", "Widgets/ButtonQuitToMenuHovered");
    _goToMainMenuButton->Move(0.5f - _goToMainMenuButton->GetSize().X / 2.0f, 0.02f);
    _goToMainMenuButton->Click.AddHandler(_goToMainMenuButtonClickHandler);
    _frame->AddChild(_goToMainMenuButton);

    _inputReceiver = new OutroInputReceiver(DomainName);

    _camera = new Camera();
    _camera->Set2DPosition(0.0f, 0.0f, 0.0f);

    Console->ReduceOffset(2);
    Console->Notice(string::Format("{0} initialized", Name));
}

//===========================================================================//

Outro::~Outro()
{
    Console->Notice(string::Format("{0} shutting down...", Name));
    Console->AddOffset(2);

    _camera = NullPtr;
    _inputReceiver = NullPtr;

    _goToMainMenuButton = NullPtr;
    _frame = NullPtr;

    Renderer->RemoveLayer(DomainName, LayerName);
    DomainManager->Remove(DomainName);

    Console->ReduceOffset(2);
    Console->Notice(string::Format("{0} has shut down", Name));
}

//===========================================================================//

void Outro::Select()
{
    Console->Notice(string::Format("{0} selected", Name));

    Scene->ApplyCamera(_camera);
    GUIManager->ShowCursor();
    DomainManager->Select(DomainName);
}

//===========================================================================//

void Outro::OnGoToMainMenuButtonClick(const MouseMessage &)
{
    Game->AddStateToQueue(GameState::MainMenu);
}

//===========================================================================//

} // namespace Citadel
