//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#ifndef OUTRO_H
#define OUTRO_H

#include "../../BruTech/Source/Helpers/Engine/Engine.h"

#include "../Game/IGameState.h"
#include "OutroInputReceiver.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class Outro: public IGameState
{
public:
    static const string Name;
    static const string DomainName;
    static const string LayerName;

    Outro();
    virtual ~Outro();

    virtual void Select();

private:
    void OnGoToMainMenuButtonClick(const MouseMessage &);

    SharedPtr<GUIFrame> _frame;
    SharedPtr<Button> _goToMainMenuButton;

    SharedPtr<OutroInputReceiver> _inputReceiver;
    SharedPtr<Camera> _camera;

    SharedPtr<EventHandler<const MouseMessage &>> _goToMainMenuButtonClickHandler;
};

//===========================================================================//

} // namespace Citadel

#endif // OUTRO_H
