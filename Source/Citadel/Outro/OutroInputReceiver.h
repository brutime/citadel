//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef OUTRO_INPUT_RECEIVER_H
#define OUTRO_INPUT_RECEIVER_H

#include "../../BruTech/Source/Helpers/Engine/Engine.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class OutroInputReceiver: public AbstractInputReceiver
{
public:
    static const string Type;

    OutroInputReceiver(const string & domainName);
    virtual ~OutroInputReceiver();

    virtual void DispatchKeyboardMessage(const KeyboardMessage & message);
    virtual void DispatchMouseMessage(const MouseMessage &) {}
    virtual void DispatchJoystickMessage(const JoystickMessage &) {}

private:
    OutroInputReceiver(const OutroInputReceiver &) = delete;
    OutroInputReceiver & operator =(const OutroInputReceiver &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // OUTRO_INPUT_RECEIVER_H
