//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "CitadelApplication.h"

#include "Game/Game.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

const integer CitadelApplication::SurfaceWidth = 1024;
const integer CitadelApplication::SurfaceHeight = 576;

const integer CitadelApplication::SurfaceCoordsWidth = 32;
const integer CitadelApplication::SurfaceCoordsHeight = 18;

//===========================================================================//

CitadelApplication::CitadelApplication() : AbstractApplication()
{
}

//===========================================================================//

CitadelApplication::~CitadelApplication()
{
}

//===========================================================================//

void CitadelApplication::Initialize()
{
    string windowTitle = string::Format("{0} {1}", GameSingleton::Name, GameSingleton::Version);
    Engine = new EngineSingleton(SurfaceWidth, SurfaceHeight, SurfaceCoordsWidth, SurfaceCoordsHeight,
                                 FullScreenMode::Off, BatchingMode::On, Colors::Black);
    Game = new GameSingleton();
}

//===========================================================================//

void CitadelApplication::MainLoop()
{
    Game->UpdateStatesQueue();
    Engine->Update();
}

//===========================================================================//

void CitadelApplication::FreeResources()
{
    Game = NullPtr;
    Engine = NullPtr;
}

//===========================================================================//

} // namespace Citadel
