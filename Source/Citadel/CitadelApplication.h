//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//     Описание:
//

#pragma once
#ifndef CITADEL_APPLICATION_H
#define CITADEL_APPLICATION_H

#include "../BruTech/Source/Helpers/Engine/Engine.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class CitadelApplication: public AbstractApplication
{
public:
    static const integer SurfaceWidth;
    static const integer SurfaceHeight;

    static const integer SurfaceCoordsWidth;
    static const integer SurfaceCoordsHeight;

    CitadelApplication();
    virtual ~CitadelApplication();

    void Initialize();
    void MainLoop();
    void FreeResources();

private:
    CitadelApplication(const CitadelApplication &) = delete;
    CitadelApplication & operator =(const CitadelApplication &) = delete;
};

//===========================================================================//

} // namespace Citadel

#endif // CITADEL_APPLICATION_H
