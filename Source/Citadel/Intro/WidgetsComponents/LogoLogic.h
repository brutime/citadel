//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Логика показа логотипа при старте игры
//

#ifndef LOGO_LOGIC_H
#define LOGO_LOGIC_H

#include "../../../BruTech/Source/Helpers/Engine/Engine.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class LogoLogic: public AbstractUpdatable
{
public:
    static const string Type;

    LogoLogic(const string & domainName, float showingTime);
    virtual ~LogoLogic();

    virtual void Reset();

    virtual void Update(float deltaTime);

    bool IsDone() const { return _isDone; }

private:
    float _showingTime;
    float _currentTime;
    bool _isDone;

    LogoLogic(const LogoLogic &) = delete;
    LogoLogic & operator =(const LogoLogic &) = delete;
};

//===========================================================================//

} // namespace Citadel

#endif // LOGO_LOGIC_H
