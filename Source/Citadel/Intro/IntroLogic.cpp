//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "IntroLogic.h"

#include "../CitadelApplication.h"
#include "../Common/UpdatingOrderTable.h"
#include "../Common/EntityManager.h"
#include "../Game/Game.h"
#include "Intro.h"
#include "WidgetsComponents/LogoLogic.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

const string IntroLogic::Type = "IntroLogic";

//===========================================================================//

IntroLogic::IntroLogic(const string & domainName, const List<string> & pathesToLogos):
    AbstractUpdatable(domainName, Type, UpdatingOrderTable->GetOrder(Type)),
    _pathesToLogos(pathesToLogos)
{
    for (const string & pathToLogo : _pathesToLogos)
    {
        EntityManager->ReserveEntities(domainName, Intro::LogosGroupName, pathToLogo, Intro::LayerName, Vector3(), 1);
    }

    if (!_pathesToLogos.IsEmpty())
    {
        EntityManager->CreateEntity(domainName, Intro::LogosGroupName, _pathesToLogos.GetFirst(),
                                    Intro::LayerName, GetLogoPosition());
    }
}

//===========================================================================//

IntroLogic::~IntroLogic()
{
}

//===========================================================================//

void IntroLogic::Reset()
{
}

//===========================================================================//

void IntroLogic::Update(float)
{
    if (_pathesToLogos.IsEmpty())
    {
        Game->GetIntro()->Finish();
        return;
    }

    const SharedPtr<Entity> & currentLogo = EntityManager->GetGroup(Intro::LogosGroupName).GetFirst();
    SharedPtr<LogoLogic> logoLogic = currentLogo->GetComponent<LogoLogic>(LogoLogic::Type);
    if (logoLogic->IsDone())
    {
        EntityManager->Remove(currentLogo);
        _pathesToLogos.Remove(_pathesToLogos.GetFirst());

        if (!_pathesToLogos.IsEmpty())
        {
            EntityManager->CreateEntity(GetDomainName(), Intro::LogosGroupName,
                                        _pathesToLogos.GetFirst(), Intro::LayerName, GetLogoPosition());
        }
    }
}

//===========================================================================//

Vector3 IntroLogic::GetLogoPosition() const
{
    return Vector3(CitadelApplication::SurfaceCoordsWidth / 2.0f, CitadelApplication::SurfaceCoordsHeight / 2.0f, 0.0f);
}

//===========================================================================//

} // namespace Citadel
