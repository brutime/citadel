//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Показ лого Brutime и BruTech
//

#ifndef INTRO_LOGIC_H
#define INTRO_LOGIC_H

#include "../../BruTech/Source/Helpers/Engine/Engine.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class IntroLogic: public AbstractUpdatable
{
public:
    static const string Type;

    IntroLogic(const string & domainName, const List<string> & pathesToLogos);
    virtual ~IntroLogic();

    virtual void Reset();

    virtual void Update(float deltaTime);

private:
    Vector3 GetLogoPosition() const;

    List<string> _pathesToLogos;

    IntroLogic(const IntroLogic &) = delete;
    IntroLogic & operator =(const IntroLogic &) = delete;
};

//===========================================================================//

} // namespace Citadel

#endif // INTRO_LOGIC_H
