//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Показ лого Brutime и BruTech
//

#ifndef INTRO_H
#define INTRO_H

#include "../../BruTech/Source/Helpers/Engine/Engine.h"

#include "../Game/IGameState.h"
#include "IntroLogic.h"
#include "IntroInputReceiver.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class Intro: public IGameState
{
public:
    static const string Name;
    static const string DomainName;
    static const string LayerName;
    static const string LogosGroupName;

    Intro();
    virtual ~Intro();

    virtual void Select();
    virtual void Leave();

    void Finish();

private:
    SharedPtr<IntroLogic> _introLogic;
    SharedPtr<IntroInputReceiver> _inputReceiver;
    SharedPtr<Camera> _camera;
};

//===========================================================================//

} // namespace Citadel

#endif // INTRO_H
