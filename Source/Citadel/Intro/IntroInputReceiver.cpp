//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "IntroInputReceiver.h"

#include "../Game/Game.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

const string IntroInputReceiver::Type = "IntroInputReceiver";

//===========================================================================//

IntroInputReceiver::IntroInputReceiver(const string & domainName): AbstractInputReceiver(domainName, Type)
{
}

//===========================================================================//

IntroInputReceiver::~IntroInputReceiver()
{
}

//===========================================================================//

void IntroInputReceiver::DispatchKeyboardMessage(const KeyboardMessage & message)
{
    if (message.State == KeyState::Down)
    {
        switch (message.Key)
        {
        case KeyboardKey::Escape:
        case KeyboardKey::Enter:
        case KeyboardKey::Space:
            Game->GetIntro()->Finish();
            break;

        default:
            break;
        }
    }
}

//===========================================================================//

void IntroInputReceiver::DispatchMouseMessage(const MouseMessage & message)
{
    if (message.State == KeyState::Down)
    {
        switch (message.Key)
        {
        case MouseKey::Left:
        case MouseKey::Middle:
        case MouseKey::Right:
            Game->GetIntro()->Finish();
            break;

        default:
            break;
        }
    }
}

//===========================================================================//

} // namespace Bru
