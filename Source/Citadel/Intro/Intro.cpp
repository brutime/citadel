//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Intro.h"

#include "../Common/EntityManager.h"
#include "../CitadelApplication.h"
#include "../Game/Game.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//


const string Intro::Name = "Intro state";
const string Intro::DomainName = "Intro";
const string Intro::LayerName = "Intro";
const string Intro::LogosGroupName = "Logos";

//===========================================================================//

Intro::Intro(): IGameState(),
    _introLogic(),
    _inputReceiver(),
    _camera()
{
    Console->Notice(string::Format("{0} initializing...", Name));
    Console->AddOffset(2);

    DomainManager->Add(DomainName);

    Renderer->AddLayerToFront(DomainName, new RenderableLayer(LayerName));

    EntityManager->AddGroup(LogosGroupName);

    _introLogic = new IntroLogic(DomainName, { "Widgets/BrutimeLogo", "Widgets/BruTechLogo" });
    _inputReceiver = new IntroInputReceiver(DomainName);

    _camera = new Camera();
    _camera->Set2DPosition(CitadelApplication::SurfaceCoordsWidth / 2.0f,
                           CitadelApplication::SurfaceCoordsHeight / 2.0f,
                           CitadelApplication::SurfaceCoordsHeight / 2.0f);

    Console->ReduceOffset(2);
    Console->Notice(string::Format("{0} initialized", Name));
}

//===========================================================================//

Intro::~Intro()
{
    Console->Notice(string::Format("{0} shutting down...", Name));
    Console->AddOffset(2);

    _camera = NullPtr;
    _inputReceiver = NullPtr;
    _introLogic = NullPtr;

    EntityManager->RemoveGroup(LogosGroupName);

    Renderer->RemoveLayer(DomainName, LayerName);
    DomainManager->Remove(DomainName);

    Console->ReduceOffset(2);
    Console->Notice(string::Format("{0} has shut down", Name));
}

//===========================================================================//

void Intro::Select()
{
    Console->Notice(string::Format("{0} selected", Name));

    Scene->ApplyCamera(_camera);
    GUIManager->HideCursor();
    DomainManager->Select(DomainName);
}

//===========================================================================//

void Intro::Leave()
{
    _inputReceiver->Disable();
}

//===========================================================================//

void Intro::Finish()
{
    Game->AddStateToQueue(GameState::MainMenu);
}

//===========================================================================//

} // namespace Citadel
