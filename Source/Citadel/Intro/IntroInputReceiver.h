//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef INTRO_INPUT_RECEIVER_H
#define INTRO_INPUT_RECEIVER_H

#include "../../BruTech/Source/Helpers/Engine/Engine.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class IntroInputReceiver: public AbstractInputReceiver
{
public:
    static const string Type;

    IntroInputReceiver(const string & domainName);
    virtual ~IntroInputReceiver();

    virtual void DispatchKeyboardMessage(const KeyboardMessage & message);
    virtual void DispatchMouseMessage(const MouseMessage & message);
    virtual void DispatchJoystickMessage(const JoystickMessage &) {}

private:
    IntroInputReceiver(const IntroInputReceiver &) = delete;
    IntroInputReceiver & operator =(const IntroInputReceiver &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // INTRO_INPUT_RECEIVER_H
