//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "TutorialInputReceiver.h"

#include "../Game/Game.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

const string TutorialInputReceiver::Type = "TutorialInputReceiver";

//===========================================================================//

TutorialInputReceiver::TutorialInputReceiver(const string & domainName): AbstractInputReceiver(domainName, Type)
{
}

//===========================================================================//

TutorialInputReceiver::~TutorialInputReceiver()
{
}

//===========================================================================//

void TutorialInputReceiver::DispatchKeyboardMessage(const KeyboardMessage & message)
{
    if (message.State == KeyState::Down)
    {
        switch (message.Key)
        {
        case KeyboardKey::Escape:
        case KeyboardKey::Enter:
        case KeyboardKey::Space:
            Game->AddStateToQueue(GameState::NewGame);
            break;

        default:
            break;
        }
    }
}

//===========================================================================//

void TutorialInputReceiver::DispatchMouseMessage(const MouseMessage & message)
{
    if (message.State == KeyState::Down)
    {
        switch (message.Key)
        {
        case MouseKey::Left:
        case MouseKey::Middle:
        case MouseKey::Right:
            Game->AddStateToQueue(GameState::NewGame);
            break;

        default:
            break;
        }
    }
}

//===========================================================================//

} // namespace Bru
