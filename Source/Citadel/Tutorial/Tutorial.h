//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#ifndef TUTORIAL_H
#define TUTORIAL_H

#include "../../BruTech/Source/Helpers/Engine/Engine.h"

#include "../Game/IGameState.h"
#include "TutorialLogic.h"
#include "TutorialInputReceiver.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class Tutorial: public IGameState
{
public:
    static const string Name;
    static const string DomainName;
    static const string LayerName;

    Tutorial();
    virtual ~Tutorial();

    virtual void Select();

private:
    void OnStartGameButtonClick(const MouseMessage &);

    SharedPtr<GUIFrame> _frame;

    SharedPtr<TutorialLogic> _tutorialLogic;
    SharedPtr<TutorialInputReceiver> _inputReceiver;
    SharedPtr<Camera> _camera;
};

//===========================================================================//

} // namespace Citadel

#endif // TUTORIAL_H
