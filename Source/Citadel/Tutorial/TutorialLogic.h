//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Экран обучения
//

#ifndef TUTORIAL_LOGIC_H
#define TUTORIAL_LOGIC_H

#include "../../BruTech/Source/Helpers/Engine/Engine.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class TutorialLogic: public AbstractUpdatable
{
public:
    static const string Type;

    TutorialLogic(const string & domainName);
    virtual ~TutorialLogic();

    virtual void Reset();

    virtual void Update(float deltaTime);

private:
    static const float WaitingTime;

    float _currentTime;

    TutorialLogic(const TutorialLogic &) = delete;
    TutorialLogic & operator =(const TutorialLogic &) = delete;
};

//===========================================================================//

} // namespace Citadel

#endif // TUTORIAL_LOGIC_H
