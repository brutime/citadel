//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "TutorialLogic.h"

#include "../Common/UpdatingOrderTable.h"
#include "../Game/Game.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

const string TutorialLogic::Type = "TutorialLogic";

//===========================================================================//

const float TutorialLogic::WaitingTime = 3.0f;

//===========================================================================//

TutorialLogic::TutorialLogic(const string & domainName):
    AbstractUpdatable(domainName, Type, UpdatingOrderTable->GetOrder(Type)),
    _currentTime(0.0f)
{
}

//===========================================================================//

TutorialLogic::~TutorialLogic()
{
}

//===========================================================================//

void TutorialLogic::Reset()
{
    _currentTime = 0.0f;
    AbstractUpdatable::Reset();
}

//===========================================================================//

void TutorialLogic::Update(float deltaTime)
{
    _currentTime += deltaTime;
    if (_currentTime > WaitingTime)
    {
        _currentTime = 0.0f;
        Game->AddStateToQueue(GameState::NewGame);
    }
}

//===========================================================================//

} // namespace Citadel
