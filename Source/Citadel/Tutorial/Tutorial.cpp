//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Tutorial.h"

#include "../Game/Game.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

const string Tutorial::Name = "Tutorial state";
const string Tutorial::DomainName = "Tutorial";
const string Tutorial::LayerName = "Tutorial";

//===========================================================================//

Tutorial::Tutorial(): IGameState(),
    _frame(),
    _tutorialLogic(),
    _inputReceiver(),
    _camera()
{
    Console->Notice(string::Format("{0} initializing...", Name));
    Console->AddOffset(2);

    DomainManager->Add(DomainName);

    Renderer->AddLayerToFront(DomainName, new RenderableLayer(LayerName, ProjectionMode::Ortho));

    _frame = new GUIFrame(DomainName, LayerName, 1.0f, 1.0f, "Widgets/TutorialBackground");

    _tutorialLogic = new TutorialLogic(DomainName);
    _inputReceiver = new TutorialInputReceiver(DomainName);

    _camera = new Camera();
    _camera->Set2DPosition(0.0f, 0.0f, 0.0f);

    Console->ReduceOffset(2);
    Console->Notice(string::Format("{0} initialized", Name));
}

//===========================================================================//

Tutorial::~Tutorial()
{
    Console->Notice(string::Format("{0} shutting down...", Name));
    Console->AddOffset(2);

    _camera = NullPtr;
    _inputReceiver = NullPtr;
    _tutorialLogic = NullPtr;

    _frame = NullPtr;

    Renderer->RemoveLayer(DomainName, LayerName);
    DomainManager->Remove(DomainName);

    Console->ReduceOffset(2);
    Console->Notice(string::Format("{0} has shut down", Name));
}

//===========================================================================//

void Tutorial::Select()
{
    Console->Notice(string::Format("{0} selected", Name));

    _tutorialLogic->Reset();
    Scene->ApplyCamera(_camera);
    GUIManager->ShowCursor();
    DomainManager->Select(DomainName);
}

//===========================================================================//

} // namespace Citadel
