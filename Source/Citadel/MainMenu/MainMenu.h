//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Главное меню
//

#ifndef MAIN_MENU_H
#define MAIN_MENU_H

#include "../../BruTech/Source/Helpers/Engine/Engine.h"

#include "../Game/IGameState.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class MainMenu: public IGameState
{
public:
    static const string Name;
    static const string DomainName;
    static const string LayerName;

    MainMenu();
    virtual ~MainMenu();

    virtual void Select();

private:
    void OnContinueGameButtonClick(const MouseMessage &);
    void OnNewGameButtonClick(const MouseMessage &);
    void OnQuitButtonClick(const MouseMessage &);

    SharedPtr<GUIFrame> _frame;
    SharedPtr<Button> _continueGameButton;
    SharedPtr<Button> _newGameButton;
    SharedPtr<Button> _quitButton;
    SharedPtr<Camera> _camera;

    SharedPtr<EventHandler<const MouseMessage &>> _continueGameButtonClickHandler;
    SharedPtr<EventHandler<const MouseMessage &>> _newGameButtonClickHandler;
    SharedPtr<EventHandler<const MouseMessage &>> _quitButtonClickHandler;
};

//===========================================================================//

} // namespace Citadel

#endif // MAIN_MENU_H
