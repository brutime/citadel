//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "MainMenu.h"

#include "../Game/Game.h"
#include "../Gameplay/Gameplay.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

const string MainMenu::Name = "Main menu";
const string MainMenu::DomainName = "MainMenu";
const string MainMenu::LayerName = "MainMenu";

//===========================================================================//

MainMenu::MainMenu(): IGameState(),
    _frame(),
    _continueGameButton(),
    _newGameButton(),
    _quitButton(),
    _camera(),
    _continueGameButtonClickHandler(new EventHandler<const MouseMessage &>(this, &MainMenu::OnContinueGameButtonClick)),
    _newGameButtonClickHandler(new EventHandler<const MouseMessage &>(this, &MainMenu::OnNewGameButtonClick)),
    _quitButtonClickHandler(new EventHandler<const MouseMessage &>(this, &MainMenu::OnQuitButtonClick))
{
    Console->Notice(string::Format("{0} initializing...", Name));
    Console->AddOffset(2);

    DomainManager->Add(DomainName);

    Renderer->AddLayerToFront(DomainName, new RenderableLayer(LayerName, ProjectionMode::Ortho));

    _frame = new GUIFrame(DomainName, LayerName, 1.0f, 1.0f, "Widgets/Background");

    _continueGameButton = new Button(DomainName, LayerName, 0.416f, 0.2153f, "Widgets/ButtonContinueGameNormal", "Widgets/ButtonContinueGamePressed", "Widgets/ButtonContinueGameHovered");
    _continueGameButton->Move(0.5f - _continueGameButton->GetSize().X / 2.0f, 0.6882f);
    _continueGameButton->Hide();
    _continueGameButton->Click.AddHandler(_continueGameButtonClickHandler);
    _frame->AddChild(_continueGameButton);

    _newGameButton = new Button(DomainName, LayerName, 0.416f, 0.2153f, "Widgets/ButtonNewGameNormal", "Widgets/ButtonNewGamePressed", "Widgets/ButtonNewGameHovered");
    _newGameButton->Move(0.5f - _newGameButton->GetSize().X / 2.0f, 0.4f);
    _newGameButton->Click.AddHandler(_newGameButtonClickHandler);
    _frame->AddChild(_newGameButton);

    _quitButton = new Button(DomainName, LayerName, 0.3418f, 0.1771f, "Widgets/ButtonQuitNormal", "Widgets/ButtonQuitPressed", "Widgets/ButtonQuitHovered");
    _quitButton->Move(0.5f - _quitButton->GetSize().X / 2.0f, 0.15f);
    _quitButton->Click.AddHandler(_quitButtonClickHandler);
    _frame->AddChild(_quitButton);

    _camera = new Camera();
    _camera->Set2DPosition(0.0f, 0.0f, 0.0f);

    Console->ReduceOffset(2);
    Console->Notice(string::Format("{0} initialized", Name));
}

//===========================================================================//

MainMenu::~MainMenu()
{
    Console->Notice(string::Format("{0} shutting down...", Name));
    Console->AddOffset(2);

    _camera = NullPtr;
    _quitButton = NullPtr;
    _newGameButton = NullPtr;
    _continueGameButton = NullPtr;
    _frame = NullPtr;

    Renderer->RemoveLayer(DomainName, LayerName);
    DomainManager->Remove(DomainName);

    Console->ReduceOffset(2);
    Console->Notice(string::Format("{0} has shut down", Name));
}

//===========================================================================//

void MainMenu::Select()
{
    Console->Notice(string::Format("{0} selected", Name));

    _continueGameButton->SetVisible(Game->GetGameplay()->IsActive());
    Scene->ApplyCamera(_camera);
    GUIManager->ShowCursor();
    DomainManager->Select(DomainName);
}

//===========================================================================//

void MainMenu::OnContinueGameButtonClick(const MouseMessage &)
{
    Game->AddStateToQueue(GameState::Gameplay);
}

//===========================================================================//

void MainMenu::OnNewGameButtonClick(const MouseMessage &)
{
    Game->AddStateToQueue(GameState::Tutorial);
}

//===========================================================================//

void MainMenu::OnQuitButtonClick(const MouseMessage &)
{
    Application->Quit();
}

//===========================================================================//

} // namespace Citadel
