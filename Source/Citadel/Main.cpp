﻿//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//     Описание: Входная точка игры
//

#include "CitadelApplication.h"

using namespace Bru;
using namespace Citadel;

//===========================================================================//

int main(int, char const *[])
{
    Application = new CitadelApplication();
    Application->ApplicationMain();

    return 0;
}

//===========================================================================//
