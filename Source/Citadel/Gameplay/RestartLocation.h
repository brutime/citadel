//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#ifndef RESTART_LOCATION_H
#define RESTART_LOCATION_H

#include "../../BruTech/Source/Helpers/Engine/Engine.h"

#include "../Game/IGameState.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class RestartLocation: public IGameState
{
public:
    static const string Name;

    RestartLocation();
    virtual ~RestartLocation();

    virtual void Select();

private:
    RestartLocation(const RestartLocation &) = delete;
    RestartLocation & operator =(const RestartLocation &) = delete;
};

//===========================================================================//

} // namespace Citadel

#endif // RESTART_LOCATION_H
