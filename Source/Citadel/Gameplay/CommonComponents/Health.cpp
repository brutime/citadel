//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Health.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

const string Health::Type = "Health";

//===========================================================================//

Health::Health(const string & domainName, integer maxHealth):
    AbstractComponent(domainName, Type),
    _health(maxHealth),
    _maxHealth(maxHealth)
{
}

//===========================================================================//

Health::~Health()
{
}

//===========================================================================//

void Health::Reset()
{
    _health = _maxHealth;
}

//===========================================================================//

void Health::Cure(integer cure)
{
    if (IsDead())
    {
        return;
    }

    _health += cure;
    if ( _health > _maxHealth)
    {
        _health = _maxHealth;
    }
}

//===========================================================================//

void Health::Damage(integer damage)
{
    if (IsDead())
    {
        return;
    }

    _health -= damage;
    if (_health <= 0)
    {
        _health = 0;
    }
}

//===========================================================================//

} // namespace Citadel
