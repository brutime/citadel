//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//     ��������: ��������� ��������
//

#pragma once
#ifndef HEALTH_H
#define HEALTH_H

#include "../../../BruTech/Source/Helpers/Engine/Engine.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class Health: public AbstractComponent
{
public:
    static const string Type;

    Health(const string & domainName, integer maxHealth);
    virtual ~Health();

    virtual void Reset();

    void Cure(integer cure);
    void Damage(integer damage);

    integer GetPercentage() const { return Math::ToPercent(_health, _maxHealth); }

    integer GetMaxHealth() const { return _maxHealth; }
    integer GetHealth() const { return _health; }
    bool IsDead() const { return _health == 0; }

private:
    integer _health;
    integer _maxHealth;

    Health(const Health &);
    Health & operator =(const Health &);
};

//===========================================================================//

} // namespace Citadel

#endif // HEALTH_H
