//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//     ��������: ��������� ��������
//

#pragma once
#ifndef ATTACK_H
#define ATTACK_H

#include "../../../BruTech/Source/Helpers/Engine/Engine.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class Attack: public AbstractComponent
{
public:
    static const string Type;

    Attack(const string & domainName, Interval power, float radius, float prepareTime);
    virtual ~Attack();

    void Damage(const WeakPtr<Entity> & target);

    float GetRadius() const { return _radius; }
    float GetPrepareTime() const { return _prepareTime; }

private:
    Interval _power;
    float _radius;
    float _prepareTime;

    Attack(const Attack &);
    Attack & operator =(const Attack &);
};

//===========================================================================//

} // namespace Citadel

#endif // ATTACK_H
