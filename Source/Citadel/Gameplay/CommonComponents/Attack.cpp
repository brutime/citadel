//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Attack.h"

#include "Health.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

const string Attack::Type = "Attack";

//===========================================================================//

Attack::Attack(const string & domainName, Interval power, float radius, float prepareTime):
    AbstractComponent(domainName, Type),
    _power(power),
    _radius(radius),
    _prepareTime(prepareTime)
{
}

//===========================================================================//

Attack::~Attack()
{
}

//===========================================================================//

void Attack::Damage(const WeakPtr<Entity> & target)
{
    if (target == NullPtr)
    {
        return;
    }

    SharedPtr<Health> health = target->GetComponent<Health>(Health::Type);
    health->Damage(_power.GetRandom());
}

//===========================================================================//

} // namespace Citadel
