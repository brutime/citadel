//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    ��������:
//

#pragma once
#ifndef FLIPPER_H
#define FLIPPER_H

#include "../../../BruTech/Source/Helpers/Engine/Engine.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class Flipper: public AbstractUpdatable
{
public:
    static const string Type;

    Flipper(const string & domainName);
    virtual ~Flipper();

    virtual void Update(float deltaTime);

private:
    Flipper(const Flipper &) = delete;
    Flipper & operator =(const Flipper &) = delete;
};


//===========================================================================//

} // namespace Citadel

#endif // FLIPPER_H
