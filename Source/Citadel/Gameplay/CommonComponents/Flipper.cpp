//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Flipper.h"

#include "../../Common/UpdatingOrderTable.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

const string Flipper::Type = "Flipper";

//===========================================================================//

Flipper::Flipper(const string & domainName):
    AbstractUpdatable(domainName, Type, UpdatingOrderTable->GetOrder(Type))
{
}

//===========================================================================//

Flipper::~Flipper()
{
}

//===========================================================================//

void Flipper::Update(float)
{
    SharedPtr<MovementToPointController> movementController =
        GetOwner()->GetComponent<MovementToPointController>(MovementToPointController::Type);

    if (movementController->GetOrientation().X < 0.0f)
    {
        SharedPtr<AbstractSprite> sprite = GetOwner()->GetComponent<AbstractSprite>(AbstractRenderable::Type);
        sprite->FlipHorizontal();
    }
    else if (movementController->GetOrientation().X > 0.0f)
    {
        SharedPtr<AbstractSprite> sprite = GetOwner()->GetComponent<AbstractSprite>(AbstractRenderable::Type);
        sprite->UnflipHorizontal();
    }
}

//===========================================================================//

} // namespace Citadel
