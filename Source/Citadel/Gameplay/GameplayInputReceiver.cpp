//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "GameplayInputReceiver.h"

#include "../Game/Game.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

const string GameplayInputReceiver::Type = "GameplayInputReceiver";

//===========================================================================//

GameplayInputReceiver::GameplayInputReceiver(const string & domainName): AbstractInputReceiver(domainName, Type)
{
}

//===========================================================================//

GameplayInputReceiver::~GameplayInputReceiver()
{
}

//===========================================================================//

void GameplayInputReceiver::DispatchKeyboardMessage(const KeyboardMessage & message)
{
    if (message.State == KeyState::Down)
    {
        switch (message.Key)
        {
        case KeyboardKey::Escape:
            Game->AddStateToQueue(GameState::MainMenu);
            break;

        default:
            break;
        }
    }
}

//===========================================================================//

} // namespace Citalde
