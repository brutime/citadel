//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#ifndef NEXT_LOCATION_H
#define NEXT_LOCATION_H

#include "../../BruTech/Source/Helpers/Engine/Engine.h"

#include "../Game/IGameState.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class NextLocation: public IGameState
{
public:
    static const string Name;

    NextLocation();
    virtual ~NextLocation();

    virtual void Select();

private:
    NextLocation(const NextLocation &) = delete;
    NextLocation & operator =(const NextLocation &) = delete;
};

//===========================================================================//

} // namespace Citadel

#endif // NEXT_LOCATION_H
