//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс отвечает за реализацию геймплея
//

#ifndef GAMEPLAY_H
#define GAMEPLAY_H

#include "../../BruTech/Source/Helpers/Engine/Engine.h"

#include "../Game/IGameState.h"
#include "Menus/GameOverMenu.h"
#include "Menus/LocationCompleteMenu.h"
#include "Logic/Bomber.h"
#include "Logic/Undertaker.h"
#include "Logic/EndLocationChecker.h"
#include "Location.h"
#include "GameplayInputReceiver.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class Gameplay: public IGameState
{
public:
    static const string Name;
    static const string DomainName;
    static const string GameplayLayerName;
    static const string HUDLayerName;
    static const string GUILayerName;

    static const string DecorationsGroupName;
    static const string BuildingsGroupName;
    static const string EnemiesGroupName;
    static const string BombsGroupName;
    static const string ExplosionsGroupName;
    static const string PlayersGroupName;
    static const string HUDGroupName;

    static const string BehaviorMask;
    static const string EnemyRespawnMask;

    static const float MovementAccuracy;

    static const string PathToBomb;
    static const string PathToExplosion;

    Gameplay();
    virtual ~Gameplay();

    virtual void Select();
    virtual void Leave();

    bool IsActive() const;

    void NewGame();
    void LoadLocation(const string & pathToLocation);
    void NextLocation();
    void LocationComplete();
    void GameOver();
    void DestroyCurrentLocation();

    bool HasNextLocation();

    const SharedPtr<LocationCompleteMenu> & GetLocationCompleteMenu() const { return _locationCompleteMenu; }
    const SharedPtr<GameOverMenu> & GetGameOverMenu() const { return _gameOverMenu; }

    const SharedPtr<Location> & GetCurrentLocation() const { return _currentLocation; }

private:
    static const string PathToLocations;
    static const string PathToPathToLocationKey;

    void LoadLocationNames();

    void EnablePlayersInput();
    void DisablePlayersInput();

    integer GetIndexOfLocationName(const string & locationName) const;

    void PrintWavesInfo() const;

    void LoadLocationCommand(const string & pathToLocation);

    void RegisterCommands();
    void UnregisterCommands();

    SharedPtr<LocationCompleteMenu> _locationCompleteMenu;
    SharedPtr<GameOverMenu> _gameOverMenu;

    SharedPtr<Bomber> _bomber;
    SharedPtr<Undertaker> _undertaker;
    SharedPtr<EndLocationChecker> _endLocationChecker;

    Array<string> _locationNames;
    SharedPtr<Location> _currentLocation;

    SharedPtr<GameplayInputReceiver> _inputReceiver;

    SharedPtr<Camera> _camera;

    Gameplay(const Gameplay &) = delete;
    Gameplay & operator =(const Gameplay &) = delete;
};

//===========================================================================//

} // namespace Citadel

#endif // GAMEPLAY_H
