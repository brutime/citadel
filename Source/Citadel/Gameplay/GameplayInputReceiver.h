//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef GAMEPLAY_INPUT_RECEIVER_H
#define GAMEPLAY_INPUT_RECEIVER_H

#include "../../BruTech/Source/Helpers/Engine/Engine.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class GameplayInputReceiver: public AbstractInputReceiver
{
public:
    static const string Type;

    GameplayInputReceiver(const string & domainName);
    virtual ~GameplayInputReceiver();

    virtual void DispatchKeyboardMessage(const KeyboardMessage & message);
    virtual void DispatchMouseMessage(const MouseMessage &) {}
    virtual void DispatchJoystickMessage(const JoystickMessage &) {}

private:
    GameplayInputReceiver(const GameplayInputReceiver &) = delete;
    GameplayInputReceiver & operator =(const GameplayInputReceiver &) = delete;
};

//===========================================================================//

} // namespace Citadel

#endif // GAMEPLAY_INPUT_RECEIVER_H
