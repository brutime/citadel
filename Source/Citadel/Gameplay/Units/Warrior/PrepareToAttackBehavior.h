//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef PREPARE_TO_ATTACK_BEHAVIOR_H
#define PREPARE_TO_ATTACK_BEHAVIOR_H

#include "../../../../BruTech/Source/Helpers/Engine/Engine.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class PrepareToAttackBehavior: public AbstractAction
{
public:
    PrepareToAttackBehavior(float prepareTime);
    virtual ~PrepareToAttackBehavior();

    virtual void Initialize();
    virtual bool Update(float deltaTime);

private:
    float _currentTime;
    float _prepareTime;

    PrepareToAttackBehavior(const PrepareToAttackBehavior &) = delete;
    PrepareToAttackBehavior& operator =(const PrepareToAttackBehavior &) = delete;
};


//===========================================================================//

} // namespace Citadel

#endif // PREPARE_TO_ATTACK_BEHAVIOR_H
