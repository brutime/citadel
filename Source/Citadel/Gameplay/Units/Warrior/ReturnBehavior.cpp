//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "ReturnBehavior.h"

#include "../../../Game/Game.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

ReturnBehavior::ReturnBehavior():
    AbstractAction(Gameplay::BehaviorMask, TaskBlockingState::Blocking, TaskInitializingState::ReInitializing)
{
}

//===========================================================================//

ReturnBehavior::~ReturnBehavior()
{
}

//===========================================================================//

void ReturnBehavior::Initialize()
{
    //OSConsoleStream->WriteLine(string::Format("{0} {1} ReturnBehavior", GetOwner()->GetName(), GetList()->GetName()));

    SharedPtr<AnimationController> animationController = GetOwner()->GetComponent<AnimationController>(AnimationController::Type);
    animationController->SelectAnimation("Move");
    animationController->Play();

    SharedPtr<MovementToPointController> movementController =
        GetOwner()->GetComponent<MovementToPointController>(MovementToPointController::Type);
    movementController->SetPoint(Vector3(-5.0f, Game->GetGameplay()->GetCurrentLocation()->GetLandLineHeight()));
    movementController->Enable();

    AbstractAction::Initialize();
}

//===========================================================================//

bool ReturnBehavior::Update(float)
{
    SharedPtr<MovementToPointController> movementController =
        GetOwner()->GetComponent<MovementToPointController>(MovementToPointController::Type);

    if (movementController->IsPointReached())
    {
        SharedPtr<AnimationController> animationController = GetOwner()->GetComponent<AnimationController>(AnimationController::Type);
        animationController->Stop();
    }

    return false;
}

//===========================================================================//

} // namespace Citadel
