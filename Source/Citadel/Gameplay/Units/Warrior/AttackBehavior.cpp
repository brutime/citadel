//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "AttackBehavior.h"

#include "../../Gameplay.h"
#include "../../CommonComponents/Attack.h"
#include "../../CommonComponents/Health.h"
#include "PrepareToAttackBehavior.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

AttackBehavior::AttackBehavior(const WeakPtr<Entity> & target):
    AbstractAction(Gameplay::BehaviorMask, TaskBlockingState::Blocking, TaskInitializingState::NotReInitializing),
    _target(target)
{
}

//===========================================================================//

AttackBehavior::~AttackBehavior()
{
}

//===========================================================================//

void AttackBehavior::Initialize()
{
    //OSConsoleStream->WriteLine(string::Format("{0} {1} AttackBehavior", GetOwner()->GetName(), GetList()->GetName()));

    SharedPtr<AnimationController> animationController = GetOwner()->GetComponent<AnimationController>(AnimationController::Type);
    animationController->SelectAnimation("Attack");
    animationController->Play();

    AbstractAction::Initialize();
}

//===========================================================================//

bool AttackBehavior::Update(float)
{
    SharedPtr<AnimationController> animationController = GetOwner()->GetComponent<AnimationController>(AnimationController::Type);

    bool result = animationController->IsFinished();
    if (result)
    {
        SharedPtr<Attack> attack = GetOwner()->GetComponent<Attack>(Attack::Type);
        attack->Damage(_target);
        SharedPtr<Health> targetHealth = _target->GetComponent<Health>(Health::Type);
        if (!targetHealth->IsDead())
        {
            GetList()->Push(new PrepareToAttackBehavior(attack->GetPrepareTime()));
        }
    }

    return result;
}

//===========================================================================//

} // namespace Citadel
