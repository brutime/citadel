//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef ATTACK_BEHAVIOR_H
#define ATTACK_BEHAVIOR_H

#include "../../../../BruTech/Source/Helpers/Engine/Engine.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class AttackBehavior: public AbstractAction
{
public:
    AttackBehavior(const WeakPtr<Entity> & target);
    virtual ~AttackBehavior();

    virtual void Initialize();
    virtual bool Update(float deltaTime);

private:
    WeakPtr<Entity> _target;

    AttackBehavior(const AttackBehavior &) = delete;
    AttackBehavior & operator =(const AttackBehavior &) = delete;
};


//===========================================================================//

} // namespace Citadel

#endif // ATTACK_BEHAVIOR_H
