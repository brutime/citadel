//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef MOVE_TO_TARGET_BEHAVIOR_H
#define MOVE_TO_TARGET_BEHAVIOR_H

#include "../../../../BruTech/Source/Helpers/Engine/Engine.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class MoveToTargetBehavior: public AbstractAction
{
public:
    MoveToTargetBehavior();
    virtual ~MoveToTargetBehavior();

    virtual void Initialize();
    virtual bool Update(float deltaTime);

private:
    void SetupMovementController();
    void StopMovementController();

    WeakPtr<Entity> _target;

    MoveToTargetBehavior(const MoveToTargetBehavior &) = delete;
    MoveToTargetBehavior & operator =(const MoveToTargetBehavior &) = delete;
};


//===========================================================================//

} // namespace Citadel

#endif // MOVE_TO_TARGET_BEHAVIOR_H
