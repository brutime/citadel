//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "PrepareToAttackBehavior.h"

#include "../../Gameplay.h"
#include "WarriorLogic.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

PrepareToAttackBehavior::PrepareToAttackBehavior(float prepareTime):
    AbstractAction(Gameplay::BehaviorMask, TaskBlockingState::Blocking, TaskInitializingState::NotReInitializing),
    _currentTime(0.0f),
    _prepareTime(prepareTime)
{
}

//===========================================================================//

PrepareToAttackBehavior::~PrepareToAttackBehavior()
{
}

//===========================================================================//

void PrepareToAttackBehavior::Initialize()
{
    //OSConsoleStream->WriteLine(string::Format("{0} {1} PrepareToAttackBehavior", GetOwner()->GetName(), GetList()->GetName()));

    SharedPtr<AnimationController> animationController = GetOwner()->GetComponent<AnimationController>(AnimationController::Type);
    animationController->SelectAnimation("Move");
    animationController->Stop();

    SharedPtr<MovementToPointController> movementController =
        GetOwner()->GetComponent<MovementToPointController>(MovementToPointController::Type);
    movementController->Disable();

    _currentTime = 0.0f;

    AbstractAction::Initialize();
}

//===========================================================================//

bool PrepareToAttackBehavior::Update(float deltaTime)
{
    _currentTime += deltaTime;
    SharedPtr<WarriorLogic> warriorLogic = GetOwner()->GetComponent<WarriorLogic>(WarriorLogic::Type);
    return warriorLogic->FindTarget() == NullPtr || _currentTime > _prepareTime;
}

//===========================================================================//

} // namespace Citadel
