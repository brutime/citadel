//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "MoveToTargetBehavior.h"

#include "../../../Common/EntityManager.h"
#include "../../../Game/Game.h"
#include "../../CommonComponents/Attack.h"
#include "../../CommonComponents/Health.h"
#include "AttackBehavior.h"
#include "WarriorLogic.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

MoveToTargetBehavior::MoveToTargetBehavior():
    AbstractAction(Gameplay::BehaviorMask, TaskBlockingState::Blocking, TaskInitializingState::ReInitializing),
    _target(NullPtr)
{
}

//===========================================================================//

MoveToTargetBehavior::~MoveToTargetBehavior()
{
}

//===========================================================================//

void MoveToTargetBehavior::Initialize()
{
    //OSConsoleStream->WriteLine(string::Format("{0} {1} MoveToTargetBehavior", GetOwner()->GetName(), GetList()->GetName()));

    SharedPtr<AnimationController> animationController = GetOwner()->GetComponent<AnimationController>(AnimationController::Type);
    animationController->SelectAnimation("Move");
    animationController->Play();

    SharedPtr<WarriorLogic> warriorLogic = GetOwner()->GetComponent<WarriorLogic>(WarriorLogic::Type);
    _target = warriorLogic->FindTarget();

    if (_target != NullPtr)
    {
        SetupMovementController();
    }
    else
    {
        StopMovementController();
    }

    AbstractAction::Initialize();
}

//===========================================================================//

bool MoveToTargetBehavior::Update(float)
{
    if (_target == NullPtr || _target->GetComponent<Health>(Health::Type)->IsDead())
    {
        SharedPtr<WarriorLogic> warriorLogic = GetOwner()->GetComponent<WarriorLogic>(WarriorLogic::Type);
        _target = warriorLogic->FindTarget();

        if (_target != NullPtr)
        {
            SetupMovementController();
        }
        else
        {
            return true;
        }
    }

    SharedPtr<MovementToPointController> movementController =
        GetOwner()->GetComponent<MovementToPointController>(MovementToPointController::Type);
    if (movementController->IsPointReached())
    {
        GetList()->Push(new AttackBehavior(_target));
    }

    return false;
}

//===========================================================================//

void MoveToTargetBehavior::SetupMovementController()
{
    SharedPtr<MovementToPointController> movementController =
        GetOwner()->GetComponent<MovementToPointController>(MovementToPointController::Type);

    SharedPtr<SceneNode> targetNode = _target->GetComponent<SceneNode>(SceneNode::Type);
    SharedPtr<Attack> attack = GetOwner()->GetComponent<Attack>(Attack::Type);
    if (targetNode->GetWorldPosition().X > attack->GetRadius())
    {
        movementController->SetPoint(Vector3(targetNode->GetWorldPosition().X - attack->GetRadius(),
                                             Game->GetGameplay()->GetCurrentLocation()->GetLandLineHeight()));
    }
    else
    {
        movementController->SetPoint(Vector3(targetNode->GetWorldPosition().X + attack->GetRadius(),
                                             Game->GetGameplay()->GetCurrentLocation()->GetLandLineHeight()));
    }
    movementController->Enable();
}

//===========================================================================//

void MoveToTargetBehavior::StopMovementController()
{
    SharedPtr<MovementToPointController> movementController =
        GetOwner()->GetComponent<MovementToPointController>(MovementToPointController::Type);

    movementController->Disable();
}

//===========================================================================//

} // namespace Citadel
