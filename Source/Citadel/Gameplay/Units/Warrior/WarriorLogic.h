//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Логика врага
//

#pragma once
#ifndef WARRIOR_LOGIC_H
#define WARRIOR_LOGIC_H

#include "../../../../BruTech/Source/Helpers/Engine/Engine.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class WarriorLogic: public AbstractUpdatable
{
public:
    static const string Type;

    WarriorLogic(const string & domainName);
    virtual ~WarriorLogic();

    virtual void Update(float deltaTime);

    WeakPtr<Entity> FindTarget() const;

private:
    WarriorLogic(const WarriorLogic &) = delete;
    WarriorLogic & operator =(const WarriorLogic &) = delete;
};

//===========================================================================//

} // namespace Citadel

#endif // WARRIOR_LOGIC_H
