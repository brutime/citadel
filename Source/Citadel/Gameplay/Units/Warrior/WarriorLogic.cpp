//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "WarriorLogic.h"

#include "../../../Common/EntityManager.h"
#include "../../../Common/UpdatingOrderTable.h"
#include "../../Gameplay.h"
#include "../../CommonComponents/Health.h"
#include "MoveToTargetBehavior.h"
#include "ReturnBehavior.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

const string WarriorLogic::Type = "WarriorLogic";

//===========================================================================//

WarriorLogic::WarriorLogic(const string & domainName):
    AbstractUpdatable(domainName, Type, UpdatingOrderTable->GetOrder(Type))
{
}

//===========================================================================//

WarriorLogic::~WarriorLogic()
{
}

//===========================================================================//

void WarriorLogic::Update(float)
{
    SharedPtr<ActionList> actionList = GetOwner()->GetComponent<ActionList>(ActionList::Type);

    if (actionList->IsEmpty())
    {
        actionList->Push(new ReturnBehavior());
        actionList->Push(new MoveToTargetBehavior());
    }
}

//===========================================================================//

WeakPtr<Entity> WarriorLogic::FindTarget() const
{
    float minDistance = MaxFloat;
    WeakPtr<Entity> nearestBuilding = NullPtr;

    const List<SharedPtr<Entity>> & buildings = EntityManager->GetGroup(Gameplay::BuildingsGroupName);
    SharedPtr<SceneNode> node = GetOwner()->GetComponent<SceneNode>(SceneNode::Type);
    for (const SharedPtr<Entity> & building : buildings)
    {
        SharedPtr<Health> buildingHealth = building->GetComponent<Health>(Health::Type);
        if (buildingHealth->IsDead())
        {
            continue;
        }

        SharedPtr<SceneNode> buildingNode = building->GetComponent<SceneNode>(SceneNode::Type);
        float distance = node->GetWorldPosition().DistanceTo(buildingNode->GetWorldPosition());

        if (distance < minDistance)
        {
            minDistance = distance;
            nearestBuilding = building;
        }
    }

    return nearestBuilding;
}

//===========================================================================//

} // namespace Citadel
