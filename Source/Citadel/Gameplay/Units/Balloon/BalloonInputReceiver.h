//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef BALLOON_INPUT_RECEIVER_H
#define BALLOON_INPUT_RECEIVER_H

#include "../../../../BruTech/Source/Helpers/Engine/Engine.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class BalloonInputReceiver: public AbstractInputReceiver
{
public:
    static const string Type;

    BalloonInputReceiver(const string & domainName);
    virtual ~BalloonInputReceiver();

    virtual void Disable();

    virtual void DispatchKeyboardMessage(const KeyboardMessage & message);
    virtual void DispatchMouseMessage(const MouseMessage & message);
    virtual void DispatchJoystickMessage(const JoystickMessage & message);

private:
    BalloonInputReceiver(const BalloonInputReceiver &) = delete;
    BalloonInputReceiver & operator =(const BalloonInputReceiver &) = delete;
};


//===========================================================================//

} // namespace Citadel

#endif // BALLOON_INPUT_RECEIVER_H
