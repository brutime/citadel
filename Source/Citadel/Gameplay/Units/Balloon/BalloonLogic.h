//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef BALLOON_LOGIC_H
#define BALLOON_LOGIC_H

#include "../../../../BruTech/Source/Helpers/Engine/Engine.h"

#include "../../Enums/MovementState.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class BalloonLogic: public AbstractUpdatable
{
public:
    static const string Type;

    BalloonLogic(const string & domainName, float speed);
    virtual ~BalloonLogic();

    virtual void Update(float deltaTime);

    void MoveLeft();
    void MoveRight();
    void Stop();

    void DropBomb();

    void SetSpeed(float speed) { _speed = speed; }
    float GetSpeed() const { return _speed; }

private:
    MovementState _movementState;
    float _speed;

    BalloonLogic(const BalloonLogic &) = delete;
    BalloonLogic & operator =(const BalloonLogic &) = delete;
};

//===========================================================================//

} // namespace Citadel

#endif // BALLOON_LOGIC_H
