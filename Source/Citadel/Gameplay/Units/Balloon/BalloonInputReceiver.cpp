﻿//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "BalloonInputReceiver.h"

#include "BalloonLogic.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

const string BalloonInputReceiver::Type = "BalloonInputReceiver";

//===========================================================================//

BalloonInputReceiver::BalloonInputReceiver(const string & domainName):
    AbstractInputReceiver(domainName, Type)
{
}

//===========================================================================//

BalloonInputReceiver::~BalloonInputReceiver()
{
}

//===========================================================================//

void BalloonInputReceiver::Disable()
{
    SharedPtr<BalloonLogic> balloonLogic = GetOwner()->GetComponent<BalloonLogic>(BalloonLogic::Type);
    balloonLogic->Stop();

    AbstractInputReceiver::Disable();
}

//===========================================================================//

void BalloonInputReceiver::DispatchKeyboardMessage(const KeyboardMessage & message)
{
    SharedPtr<BalloonLogic> balloonLogic = GetOwner()->GetComponent<BalloonLogic>(BalloonLogic::Type);
    SharedPtr<SceneNode> sceneNode = GetOwner()->GetComponent<SceneNode>(SceneNode::Type);

    if (message.State == KeyState::Down)
    {
        switch (message.Key)
        {
        case KeyboardKey::Left:
            balloonLogic->MoveLeft();
            break;

        case KeyboardKey::Right:
            balloonLogic->MoveRight();
            break;

        case KeyboardKey::Space:
            balloonLogic->DropBomb();
            break;

        default:
            break;
        }
    }
    else if (message.State == KeyState::Up)
    {
        switch (message.Key)
        {
        case KeyboardKey::Left:
            if (!Keyboard->IsKeyDown(KeyboardKey::Right))
            {
                balloonLogic->Stop();
            }
            else
            {
                balloonLogic->MoveRight();
            }
            break;

        case KeyboardKey::Right:
            if (!Keyboard->IsKeyDown(KeyboardKey::Left))
            {
                balloonLogic->Stop();
            }
            else
            {
                balloonLogic->MoveLeft();
            }
            break;

        default:
            break;
        }
    }
}

//===========================================================================//

void BalloonInputReceiver::DispatchMouseMessage(const MouseMessage &)
{
}

//===========================================================================//

void BalloonInputReceiver::DispatchJoystickMessage(const JoystickMessage &)
{
}

//===========================================================================//

} // namespace Citadel
