﻿//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "BalloonLogic.h"

#include "../../../Common/EntityManager.h"
#include "../../../Common/UpdatingOrderTable.h"
#include "../../../Game/Game.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

const string BalloonLogic::Type = "BalloonLogic";

//===========================================================================//

BalloonLogic::BalloonLogic(const string & domainName, float speed):
    AbstractUpdatable(domainName, Type, UpdatingOrderTable->GetOrder(Type)),
    _movementState(MovementState::Stand),
    _speed(speed)
{
}

//===========================================================================//

BalloonLogic::~BalloonLogic()
{
}

//===========================================================================//

void BalloonLogic::Update(float deltaTime)
{
    SharedPtr<SceneNode> node = GetOwner()->GetComponent<SceneNode>(SceneNode::Type);
    SharedPtr<AbstractSprite> sprite = GetOwner()->GetComponent<AbstractSprite>(AbstractRenderable::Type);

    float boundsLeft = Game->GetGameplay()->GetCurrentLocation()->GetBalloonBoundsLeft();
    float boundsRight = Game->GetGameplay()->GetCurrentLocation()->GetBalloonBoundsRight();

    if (_movementState == MovementState::MoveLeft)
    {
        node->Translate(-_speed * deltaTime, 0.0f);
    }
    else if (_movementState == MovementState::MoveRight)
    {
        node->Translate(_speed * deltaTime, 0.0f);
    }

    if (node->GetPosition().X - sprite->GetOrigin().X < boundsLeft)
    {
        node->SetPositionX(boundsLeft + sprite->GetOrigin().X);
    }
    else if (node->GetPosition().X + sprite->GetWidth() - sprite->GetOrigin().X > boundsRight)
    {
        node->SetPositionX(boundsRight - + sprite->GetWidth() + sprite->GetOrigin().X);
    }
}

//===========================================================================//

void BalloonLogic::MoveLeft()
{
    _movementState = MovementState::MoveLeft;
}

//===========================================================================//

void BalloonLogic::MoveRight()
{
    _movementState = MovementState::MoveRight;
}

//===========================================================================//

void BalloonLogic::Stop()
{
    _movementState = MovementState::Stand;
}

//===========================================================================//

void BalloonLogic::DropBomb()
{
    SharedPtr<SceneNode> node = GetOwner()->GetComponent<SceneNode>(SceneNode::Type);
    SharedPtr<AbstractSprite> sprite = GetOwner()->GetComponent<AbstractSprite>(AbstractRenderable::Type);
    float spriteHeight = sprite->GetHeight();
    float yOffset = spriteHeight - (spriteHeight - sprite->GetOrigin().Y) + 0.5f;
    Vector3 bombPosition = node->GetWorldPosition() - Vector3(0.0f, yOffset, 0.0f);

    EntityManager->CreateEntity(Gameplay::DomainName, Gameplay::BombsGroupName,
                                Gameplay::PathToBomb, Gameplay::GameplayLayerName, bombPosition);
}

//===========================================================================//

} // namespace Citadel
