//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "LocationCompleteMenuLogic.h"

#include "../../Common/UpdatingOrderTable.h"
#include "../../Game/Game.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

const string LocationCompleteMenuLogic::Type = "LocationCompleteMenuLogic";

//===========================================================================//

const float LocationCompleteMenuLogic::WaitingTime = 0.5f;

//===========================================================================//

LocationCompleteMenuLogic::LocationCompleteMenuLogic(const string & domainName):
    AbstractUpdatable(domainName, Type, UpdatingOrderTable->GetOrder(Type)),
    _currentTime(0.0f),
    _showingMedalsCount(0),
    _showedMedalsCount(0)
{
}

//===========================================================================//

LocationCompleteMenuLogic::~LocationCompleteMenuLogic()
{
}

//===========================================================================//

void LocationCompleteMenuLogic::Reset()
{
    _currentTime = 0.0f;
    _showedMedalsCount = 0;
    AbstractUpdatable::Reset();
}

//===========================================================================//

void LocationCompleteMenuLogic::Update(float deltaTime)
{
    _currentTime += deltaTime;
    if (_currentTime > WaitingTime)
    {
        if (_showedMedalsCount < _showingMedalsCount)
        {
            Game->GetGameplay()->GetLocationCompleteMenu()->ShowMedal(_showedMedalsCount);
            _showedMedalsCount++;
        }
        _currentTime = 0.0f;
    }
}

//===========================================================================//

} // namespace Citadel
