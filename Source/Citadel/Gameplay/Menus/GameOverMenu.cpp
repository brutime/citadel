//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "GameOverMenu.h"

#include "../../Game/Game.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

GameOverMenu::GameOverMenu(const string & domainName, const string & layerName):
    _frame(),
    _retryButton(),
    _goToMainMenuButton(),
    _inputReceiver(),
    _retryButtonClickHandler(new EventHandler<const MouseMessage &>(this, &GameOverMenu::OnRetryButtonClick)),
    _goToMainMenuButtonClickHandler(new EventHandler<const MouseMessage &>(this, &GameOverMenu::OnGoToMainMenuButtonClick))
{
    _frame = new GUIFrame(domainName, layerName, 0.625f, 0.7743f, "Widgets/WindowBackground");
    _frame->MoveTo(0.1875f, 0.11285f);

    _retryButton = new Button(domainName, layerName, 0.3418f, 0.1771f, "Widgets/ButtonRetryNormal", "Widgets/ButtonRetryPressed", "Widgets/ButtonRetryHovered");
    _frame->AddChild(_retryButton);
    _retryButton->Move(0.3125f - _retryButton->GetSize().X / 2.0f, 0.42f);
    _retryButton->Click.AddHandler(_retryButtonClickHandler);

    _goToMainMenuButton = new Button(domainName, layerName, 0.3418f, 0.1771f, "Widgets/ButtonQuitToMenuNormal", "Widgets/ButtonQuitToMenuPressed", "Widgets/ButtonQuitToMenuHovered");
    _goToMainMenuButton->Move(0.3125f - _goToMainMenuButton->GetSize().X / 2.0f, 0.20f);
    _goToMainMenuButton->Click.AddHandler(_goToMainMenuButtonClickHandler);
    _frame->AddChild(_goToMainMenuButton);

    _inputReceiver = new GameOverMenuInputReceiver(domainName);
}

//===========================================================================//

GameOverMenu::~GameOverMenu()
{
    _inputReceiver = NullPtr;
    _goToMainMenuButton = NullPtr;
    _retryButton = NullPtr;
    _frame = NullPtr;
}

//===========================================================================//

void GameOverMenu::Show()
{
    _frame->Show();
    _retryButton->Show();
    _goToMainMenuButton->Show();

    _inputReceiver->Enable();
}

//===========================================================================//

void GameOverMenu::Hide()
{
    _frame->Hide();
    _retryButton->Hide();
    _goToMainMenuButton->Hide();

    _inputReceiver->Disable();
}

//===========================================================================//

void GameOverMenu::Retry()
{
    Game->AddStateToQueue(GameState::RestartLocation);
}

//===========================================================================//

void GameOverMenu::OnRetryButtonClick(const MouseMessage &)
{
    Retry();
}

//===========================================================================//

void GameOverMenu::OnGoToMainMenuButtonClick(const MouseMessage &)
{
    Game->AddStateToQueue(GameState::MainMenu);
}

//===========================================================================//

} // namespace Citadel
