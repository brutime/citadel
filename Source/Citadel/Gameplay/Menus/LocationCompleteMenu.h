//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс отвечает за реализацию геймплея
//

#ifndef LOCATION_COMPLETE_MENU_H
#define LOCATION_COMPLETE_MENU_H

#include "../../../BruTech/Source/Helpers/Engine/Engine.h"

#include "LocationCompleteMenuLogic.h"
#include "LocationCompleteMenuInputReceiver.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class LocationCompleteMenu
{
public:
    static const integer BonusBombsCount;

    LocationCompleteMenu(const string & domainName, const string & layerName);
    ~LocationCompleteMenu();

    void SetShowingMedalsCount(integer showingMedalsCount);
    void ShowMedal(integer bombIndex);

    void Show();
    void Hide();

    void SetVisible(bool isVisible) { isVisible ? Show() : Hide(); }
    bool IsVisible() const { return _frame->IsVisible(); }

    void Next() const;

private:
    void OnNextButtonClick(const MouseMessage &);

    SharedPtr<GUIFrame> _frame;
    SharedPtr<Button> _nextButton;

    Array<SharedPtr<Entity>> _medalHoles;
    Array<SharedPtr<Entity>> _medals;
    SharedPtr<LocationCompleteMenuLogic> _locationCompleteMenuLogic;
    SharedPtr<LocationCompleteMenuInputReceiver> _inputReceiver;

    SharedPtr<EventHandler<const MouseMessage &>> _nextButtonClickHandler;

    LocationCompleteMenu(const LocationCompleteMenu &) = delete;
    LocationCompleteMenu & operator =(const LocationCompleteMenu &) = delete;
};

//===========================================================================//

} // namespace Citadel

#endif // LOCATION_COMPLETE_MENU_H
