//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Показ лого Brutime и BruTech
//

#ifndef LOCATION_COMPLETE_MENU_LOGIC_H
#define LOCATION_COMPLETE_MENU_LOGIC_H

#include "../../../BruTech/Source/Helpers/Engine/Engine.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class LocationCompleteMenuLogic: public AbstractUpdatable
{
public:
    static const string Type;

    LocationCompleteMenuLogic(const string & domainName);
    virtual ~LocationCompleteMenuLogic();

    void SetShowingMedalsCount(integer showingMedalsCount) { _showingMedalsCount = showingMedalsCount; }

    virtual void Reset();

    virtual void Update(float deltaTime);

private:
    static const float WaitingTime;

    float _currentTime;
    integer _showingMedalsCount;
    integer _showedMedalsCount;

    LocationCompleteMenuLogic(const LocationCompleteMenuLogic &) = delete;
    LocationCompleteMenuLogic & operator =(const LocationCompleteMenuLogic &) = delete;
};

//===========================================================================//

} // namespace Citadel

#endif // LOCATION_COMPLETE_MENU_LOGIC_H
