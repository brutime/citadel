//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "GameOverMenuInputReceiver.h"

#include "../../Game/Game.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

const string GameOverMenuInputReceiver::Type = "GameOverMenuInputReceiver";

//===========================================================================//

GameOverMenuInputReceiver::GameOverMenuInputReceiver(const string & domainName): AbstractInputReceiver(domainName, Type)
{
}

//===========================================================================//

GameOverMenuInputReceiver::~GameOverMenuInputReceiver()
{
}

//===========================================================================//

void GameOverMenuInputReceiver::DispatchKeyboardMessage(const KeyboardMessage & message)
{
    if (message.State == KeyState::Down)
    {
        switch (message.Key)
        {
        case KeyboardKey::Enter:
            Game->GetGameplay()->GetGameOverMenu()->Retry();
            break;

        default:
            break;
        }
    }
}

//===========================================================================//

} // namespace Citalde
