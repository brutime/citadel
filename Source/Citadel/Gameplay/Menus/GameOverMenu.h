//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс отвечает за реализацию геймплея
//

#ifndef GAME_OVER_MENU_H
#define GAME_OVER_MENU_H

#include "../../../BruTech/Source/Helpers/Engine/Engine.h"

#include "GameOverMenuInputReceiver.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class GameOverMenu
{
public:
    GameOverMenu(const string & domainName, const string & layerName);
    ~GameOverMenu();

    void Show();
    void Hide();

    void SetVisible(bool isVisible) { isVisible ? Show() : Hide(); }
    bool IsVisible() const { return _frame->IsVisible(); }

    void Retry();

private:
    void OnRetryButtonClick(const MouseMessage &);
    void OnGoToMainMenuButtonClick(const MouseMessage &);

    SharedPtr<GUIFrame> _frame;
    SharedPtr<Button> _retryButton;
    SharedPtr<Button> _goToMainMenuButton;

    SharedPtr<GameOverMenuInputReceiver> _inputReceiver;

    SharedPtr<EventHandler<const MouseMessage &>> _retryButtonClickHandler;
    SharedPtr<EventHandler<const MouseMessage &>> _goToMainMenuButtonClickHandler;

    GameOverMenu(const GameOverMenu &) = delete;
    GameOverMenu & operator =(const GameOverMenu &) = delete;
};

//===========================================================================//

} // namespace Citadel

#endif // GAME_OVER_MENU_H
