//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef LOCATION_COMPLETE_MENU_INPUT_RECEIVER_H
#define LOCATION_COMPLETE_MENU_INPUT_RECEIVER_H

#include "../../../BruTech/Source/Helpers/Engine/Engine.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class LocationCompleteMenuInputReceiver: public AbstractInputReceiver
{
public:
    static const string Type;

    LocationCompleteMenuInputReceiver(const string & domainName);
    virtual ~LocationCompleteMenuInputReceiver();

    virtual void DispatchKeyboardMessage(const KeyboardMessage & message);
    virtual void DispatchMouseMessage(const MouseMessage &) {}
    virtual void DispatchJoystickMessage(const JoystickMessage &) {}

private:
    LocationCompleteMenuInputReceiver(const LocationCompleteMenuInputReceiver &) = delete;
    LocationCompleteMenuInputReceiver & operator =(const LocationCompleteMenuInputReceiver &) = delete;
};

//===========================================================================//

} // namespace Citadel

#endif // LOCATION_COMPLETE_MENU_INPUT_RECEIVER_H
