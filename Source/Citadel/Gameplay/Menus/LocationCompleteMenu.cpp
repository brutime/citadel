//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "LocationCompleteMenu.h"

#include "../../Common/EntityManager.h"
#include "../../Game/Game.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

const integer LocationCompleteMenu::BonusBombsCount = 3;

//===========================================================================//

LocationCompleteMenu::LocationCompleteMenu(const string & domainName, const string & layerName):
    _frame(),
    _nextButton(),
    _medalHoles(3),
    _medals(3),
    _locationCompleteMenuLogic(),
    _inputReceiver(),
    _nextButtonClickHandler(new EventHandler<const MouseMessage &>(this, &LocationCompleteMenu::OnNextButtonClick))
{
    _frame = new GUIFrame(domainName, layerName, 0.625f, 0.7743f, "Widgets/WindowBackground");
    _frame->MoveTo(0.1875f, 0.11285f);

    _nextButton = new Button(domainName, layerName, 0.3418f, 0.1771f, "Widgets/ButtonNextLevelNormal", "Widgets/ButtonNextLevelPressed", "Widgets/ButtonNextLevelHovered");
    _nextButton->Move(0.3125f - _nextButton->GetSize().X / 2.0f, 0.15f);
    _nextButton->Click.AddHandler(_nextButtonClickHandler);
    _frame->AddChild(_nextButton);

    Array<Vector3> positions = {Vector3(0.33f, 0.6f), Vector3(0.5f, 0.6f), Vector3(0.67f, 0.6f)};

    for (integer i = 0; i < BonusBombsCount; i++)
    {
        _medalHoles[i] = EntityManager->CreateEntity(domainName, Gameplay::HUDGroupName, "Widgets/MedalHole",
                         layerName, positions[i]);

        SharedPtr<AbstractRenderable> renderable = _medalHoles[i]->GetComponent<AbstractRenderable>(AbstractRenderable::Type);
        renderable->Hide();
    }

    Array<string> pathesToSprites = { "Widgets/MedalBronze", "Widgets/MedalSilver", "Widgets/MedalGold" };

    for (integer i = 0; i < BonusBombsCount; i++)
    {
        _medals[i] = EntityManager->CreateEntity(domainName, Gameplay::HUDGroupName, pathesToSprites[i],
                     layerName, positions[i]);

        SharedPtr<AbstractRenderable> renderable = _medals[i]->GetComponent<AbstractRenderable>(AbstractRenderable::Type);
        renderable->Hide();
    }

    _locationCompleteMenuLogic = new LocationCompleteMenuLogic(domainName);

    _inputReceiver = new LocationCompleteMenuInputReceiver(domainName);
}

//===========================================================================//

LocationCompleteMenu::~LocationCompleteMenu()
{
    _inputReceiver = NullPtr;
    _locationCompleteMenuLogic = NullPtr;
    _medalHoles.Clear();
    _medals.Clear();
    _nextButton = NullPtr;
    _frame = NullPtr;
}

//===========================================================================//

void LocationCompleteMenu::SetShowingMedalsCount(integer showingMedalsCount)
{
    _locationCompleteMenuLogic->SetShowingMedalsCount(showingMedalsCount);
}

//===========================================================================//

void LocationCompleteMenu::ShowMedal(integer bombIndex)
{
    SharedPtr<AbstractRenderable> renderable = _medals[bombIndex]->GetComponent<AbstractRenderable>(AbstractRenderable::Type);
    renderable->Show();
}

//===========================================================================//

void LocationCompleteMenu::Show()
{
    _frame->Show();
    _nextButton->Show();

    for (integer i = 0; i < BonusBombsCount; i++)
    {
        SharedPtr<AbstractRenderable> renderable = _medalHoles[i]->GetComponent<AbstractRenderable>(AbstractRenderable::Type);
        renderable->Show();
    }

    _locationCompleteMenuLogic->Reset();
    _inputReceiver->Enable();
}

//===========================================================================//

void LocationCompleteMenu::Hide()
{
    _frame->Hide();
    _nextButton->Hide();

    for (integer i = 0; i < BonusBombsCount; i++)
    {
        SharedPtr<AbstractRenderable> medalRenderable = _medals[i]->GetComponent<AbstractRenderable>(AbstractRenderable::Type);
        medalRenderable->Hide();

        SharedPtr<AbstractRenderable> holeRenderable = _medalHoles[i]->GetComponent<AbstractRenderable>(AbstractRenderable::Type);
        holeRenderable->Hide();
    }

    _locationCompleteMenuLogic->Disable();
    _inputReceiver->Disable();
}

//===========================================================================//

void LocationCompleteMenu::Next() const
{
    if (Game->GetGameplay()->HasNextLocation())
    {
        Game->AddStateToQueue(GameState::NextLocation);
    }
    else
    {
        Game->AddStateToQueue(GameState::EndGame);
    }
}

//===========================================================================//

void LocationCompleteMenu::OnNextButtonClick(const MouseMessage &)
{
    if (Game->GetGameplay()->HasNextLocation())
    {
        Game->AddStateToQueue(GameState::NextLocation);
    }
    else
    {
        Game->AddStateToQueue(GameState::EndGame);
    }
}

//===========================================================================//

} // namespace Citadel
