//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef LOCATION_H
#define LOCATION_H

#include "../../BruTech/Source/Helpers/Engine/Engine.h"
#include "Logic/EnemyGenerator/Respawn.h"
#include "HUDComponents/LocationNameLogic.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class Location
{
public:
    Location(const string & domainName, const string & pathToFile);
    ~Location();

    const string & GetPathToFile() const { return _pathToFile; }

    float GetWidth() const { return _width; }
    float GetHeight() const { return _height; }

    float GetLandLineHeight() const { return _landLineHeight; }

    float GetBalloonBoundsLeft() const { return _balloonBoundsLeft; }
    float GetBalloonBoundsRight() const { return _balloonBoundsRight; }

    void EnableRespawns();
    void DisableRespawns();
    bool IsRespawnsEmpty();

    const List<SharedPtr<Respawn>> & GetRespawns() const { return _respawns; }

private:
    static const string NameKey;
    static const string WidthKey;
    static const string HeightKey;
    static const string LandLineHeightKey;
    static const string BalloonBoundsLeftKey;
    static const string BalloonBoundsRightKey;

    static const string PathToTextures;
    static const string PathToPathToTexture;

    static const string PathParametersFiles;
    static const string PathToPathParameterFiles;

    static const string PathToLayers;

    static const string PathToCachedEntities;
    static const string PathToEntities;

    static const string PathToRespawns;
    static const string PathToEnemiesInfo;
    static const string PathToWaves;

    static const string LayerNameKey;
    static const string GroupNameKey;
    static const string EntityPathToEntityKey;
    static const string EntityLayerNameKey;
    static const string EntityPositionKey;
    static const string EntityCountKey;
    static const string RespawnPositionKey;
    static const string EnemyInfoTypeKey;
    static const string EnemyInfoPathToEntityKey;
    static const string EnemyInfoLayerNameKey;
    static const string EnemyTypeKey;
    static const string EnemySpawnTimeKey;

    void Load();
    void Clear();

    void LoadData();
    void RemoveData();

    void LoadCachedEntities();
    void LoadEntities();
    void LoadRespawns();

    void SetupLocationName();

    string _domainName;
    string _pathToFile;
    string _fullPathToFile;

    string _name;

    float _width;
    float _height;

    float _landLineHeight;

    float _balloonBoundsLeft;
    float _balloonBoundsRight;

    List<SharedPtr<Respawn>> _respawns;
    SharedPtr<Entity> _locationName;

    Location(const Location &) = delete;
    Location & operator =(const Location &) = delete;
};


//===========================================================================//

} // namespace Citadel

#endif // LOCATION_H
