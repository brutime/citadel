//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "BombLogic.h"

#include "../../Game/Game.h"
#include "../../Common/EntityManager.h"
#include "../CommonComponents/Health.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

const string BombLogic::Type = "BombLogic";

//===========================================================================//

BombLogic::BombLogic(const string & domainName, Interval power, float radius, float fallingSpeed):
    AbstractComponent(domainName, Type),
    _power(power),
    _radius(radius),
    _fallingSpeed(fallingSpeed)
{
}

//===========================================================================//

BombLogic::~BombLogic()
{
}

//===========================================================================//

void BombLogic::Explose()
{
    integer explosePower = _power.GetRandom();
    SharedPtr<SceneNode> node = GetOwner()->GetComponent<SceneNode>(SceneNode::Type);

    const List<SharedPtr<Entity>> & enemies = EntityManager->GetGroup(Gameplay::EnemiesGroupName);
    for (const SharedPtr<Entity> & enemy : enemies)
    {
        SharedPtr<SceneNode> enemyNode = enemy->GetComponent<SceneNode>(SceneNode::Type);
        float damageFactor = 1.0f - Math::Abs(node->GetWorldPosition().X - enemyNode->GetWorldPosition().X) * 1.0f / _radius;
        integer damage = explosePower * damageFactor;
        if (damage > 0)
        {
            SharedPtr<Health> health = enemy->GetComponent<Health>(Health::Type);
            health->Damage(damage);
        }
    }
}

//===========================================================================//

} // namespace Citadel
