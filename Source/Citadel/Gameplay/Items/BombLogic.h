//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef BOMB_LOGIC_H
#define BOMB_LOGIC_H

#include "../../../BruTech/Source/Helpers/Engine/Engine.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class BombLogic: public AbstractComponent
{
public:
    static const string Type;

    BombLogic(const string & domainName, Interval power, float radius, float fallingSpeed);
    virtual ~BombLogic();

    void Explose();

    float GetFallingSpeed() const { return _fallingSpeed; }

private:
    Interval _power;
    float _radius;
    float _fallingSpeed;

    BombLogic(const BombLogic &) = delete;
    BombLogic & operator =(const BombLogic &) = delete;
};


//===========================================================================//

} // namespace Citadel

#endif // BOMB_LOGIC_H
