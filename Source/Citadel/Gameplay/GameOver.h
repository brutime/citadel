//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс отвечает за реализацию геймплея
//

#ifndef GAME_OVER_H
#define GAME_OVER_H

#include "../../BruTech/Source/Helpers/Engine/Engine.h"

#include "../Game/IGameState.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class GameOver: public IGameState
{
public:
    static const string Name;

    GameOver();
    virtual ~GameOver();

    virtual void Select();

private:
    GameOver(const GameOver &) = delete;
    GameOver & operator =(const GameOver &) = delete;
};

//===========================================================================//

} // namespace Citadel

#endif // GAME_OVER_H
