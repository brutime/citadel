//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "RestartLocation.h"

#include "../Game/Game.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

const string RestartLocation::Name = "Restart location state";

//===========================================================================//

RestartLocation::RestartLocation(): IGameState()
{
    Console->Notice(string::Format("{0} initializing...", Name));
    Console->AddOffset(2);

    Console->ReduceOffset(2);
    Console->Notice(string::Format("{0} initialized", Name));
}

//===========================================================================//

RestartLocation::~RestartLocation()
{
    Console->Notice(string::Format("{0} shutting down...", Name));
    Console->AddOffset(2);

    Console->ReduceOffset(2);
    Console->Notice(string::Format("{0} has shut down", Name));
}

//===========================================================================//

void RestartLocation::Select()
{
    Console->Notice(string::Format("{0} selected", Name));

    Game->GetLoadLocation()->SetLocationName(Game->GetGameplay()->GetCurrentLocation()->GetPathToFile());
    Game->AddStateToQueue(GameState::LoadLocation);
}

//===========================================================================//

} // namespace Citadel
