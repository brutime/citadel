//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс отвечает за реализацию геймплея
//

#ifndef NEW_GAME_H
#define NEW_GAME_H

#include "../../BruTech/Source/Helpers/Engine/Engine.h"

#include "../Game/IGameState.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class NewGame: public IGameState
{
public:
    static const string Name;

    NewGame();
    virtual ~NewGame();

    virtual void Select();

private:
    NewGame(const NewGame &) = delete;
    NewGame & operator =(const NewGame &) = delete;
};

//===========================================================================//

} // namespace Citadel

#endif // NEW_GAME_H
