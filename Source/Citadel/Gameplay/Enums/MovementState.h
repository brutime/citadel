//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    ��������:
//

#pragma once
#ifndef MOVEMENT_STATE_H
#define MOVEMENT_STATE_H

namespace Citadel
{

//===========================================================================//

enum class MovementState
{
    Stand,
    MoveLeft,
    MoveRight
};

//===========================================================================//

} // namespace Citadel

#endif // MOVEMENT_STATE_H
