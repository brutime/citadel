//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "LocationNameLogic.h"

#include "../../Common/UpdatingOrderTable.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

const string LocationNameLogic::Type = "LocationNameLogic";

//===========================================================================//

LocationNameLogic::LocationNameLogic(const string & domainName, float showingTime):
    AbstractUpdatable(domainName, Type, UpdatingOrderTable->GetOrder(Type)),
    _showingTime(showingTime),
    _currentTime(0.0f)
{
}

//===========================================================================//

LocationNameLogic::~LocationNameLogic()
{
}

//===========================================================================//

void LocationNameLogic::Reset()
{
    SharedPtr<RenderableText> renderableText = GetOwner()->GetComponent<RenderableText>(RenderableText::Type);
    SharedPtr<ColorAlphaController> colorAlphaController = GetOwner()->GetComponent<ColorAlphaController>(ColorAlphaController::Type);
    renderableText->SetText(string::Empty);
    renderableText->Show();
    colorAlphaController->Enable();
    _currentTime = 0.0f;
    AbstractUpdatable::Reset();
}

//===========================================================================//

void LocationNameLogic::Update(float deltaTime)
{
    SharedPtr<AbstractRenderable> renderable = GetOwner()->GetComponent<AbstractRenderable>(AbstractRenderable::Type);

    if (Math::EqualsZero(_currentTime) && renderable->GetColorAlpha() < 1.0f)
    {
        return;
    }

    _currentTime += deltaTime;

    SharedPtr<ColorAlphaController> colorAlphaController = GetOwner()->GetComponent<ColorAlphaController>(ColorAlphaController::Type);
    if (_currentTime > _showingTime && colorAlphaController->GetAlphaEnhanceSpeed() > 0.0f)
    {
        colorAlphaController->InverseAlphaEnhanceSpeed();
    }

    if (Math::EqualsZero(renderable->GetColorAlpha()))
    {
        renderable->Hide();
        colorAlphaController->Disable();
        Disable();
    }
}

//===========================================================================//

} // namespace Citadel
