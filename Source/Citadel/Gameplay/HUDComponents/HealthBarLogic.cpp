﻿//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "HealthBarLogic.h"

#include "../../Common/UpdatingOrderTable.h"
#include "../../Game/Game.h"
#include "../CommonComponents/Health.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

const string HealthBarLogic::Type = "HealthBarLogic";

//===========================================================================//

HealthBarLogic::HealthBarLogic(const string & domainName, float yellowZonePercent, float redZonePercent,
                               const Color & greenZoneColor, const Color & yellowZoneColor,
                               const Color & redZoneColor, float sourceSpriteWidth):
    AbstractUpdatable(domainName, Type, UpdatingOrderTable->GetOrder(Type)),
    _yellowZonePercent(yellowZonePercent),
    _redZonePercent(redZonePercent),
    _greenZoneColor(greenZoneColor),
    _yellowZoneColor(yellowZoneColor),
    _redZoneColor(redZoneColor),
    _sourceSpriteWidth(sourceSpriteWidth)
{
}

//===========================================================================//

HealthBarLogic::~HealthBarLogic()
{
}

//===========================================================================//

void HealthBarLogic::Reset()
{
    SharedPtr<AbstractSprite> sprite = GetOwner()->GetComponent<AbstractSprite>(AbstractRenderable::Type);
    sprite->SetColor(_greenZoneColor);
    sprite->SetWidth(_sourceSpriteWidth);
    AbstractUpdatable::Reset();
}

//===========================================================================//

void HealthBarLogic::Update(float)
{
    SharedPtr<Health> health = GetOwner()->GetParent()->GetComponent<Health>(Health::Type);
    SharedPtr<AbstractSprite> sprite = GetOwner()->GetComponent<AbstractSprite>(AbstractRenderable::Type);

    integer healthPercentage = health->GetPercentage();
    if (health->GetPercentage() > _yellowZonePercent)
    {
        sprite->SetColor(_greenZoneColor);
    }
    else if (health->GetPercentage() > _redZonePercent)
    {
        sprite->SetColor(_yellowZoneColor);
    }
    else
    {
        sprite->SetColor(_redZoneColor);
    }

    sprite->SetWidth(Math::FromPercent(static_cast<float>(healthPercentage), _sourceSpriteWidth));
}

//===========================================================================//

} // namespace Citadel
