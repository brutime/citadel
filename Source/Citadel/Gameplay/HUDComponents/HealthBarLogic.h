//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef HEALTH_BAR_LOGIC_H
#define HEALTH_BAR_LOGIC_H

#include "../../../BruTech/Source/Helpers/Engine/Engine.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class HealthBarLogic: public AbstractUpdatable
{
public:
    static const string Type;

    HealthBarLogic(const string & domainName, float yellowZonePercent, float redZonePercent,
                   const Color & greenZoneColor, const Color & yellowZoneColor,
                   const Color & redZoneColor, float sourceSpriteWidth);
    virtual ~HealthBarLogic();

    virtual void Reset();

    virtual void Update(float deltaTime);

private:
    float _yellowZonePercent;
    float _redZonePercent;
    Color _greenZoneColor;
    Color _yellowZoneColor;
    Color _redZoneColor;
    float _sourceSpriteWidth;

    HealthBarLogic(const HealthBarLogic &) = delete;
    HealthBarLogic & operator =(const HealthBarLogic &) = delete;
};


//===========================================================================//

} // namespace Citadel

#endif // HEALTH_BAR_LOGIC_H
