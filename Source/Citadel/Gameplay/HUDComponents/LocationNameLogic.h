//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef LOCATION_NAME_LOGIC_H
#define LOCATION_NAME_LOGIC_H

#include "../../../BruTech/Source/Helpers/Engine/Engine.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class LocationNameLogic: public AbstractUpdatable
{
public:
    static const string Type;

    LocationNameLogic(const string & domainName, float showingTime);
    virtual ~LocationNameLogic();

    virtual void Reset();

    virtual void Update(float deltaTime);

private:
    float _showingTime;
    float _currentTime;

    LocationNameLogic(const LocationNameLogic &) = delete;
    LocationNameLogic & operator =(const LocationNameLogic &) = delete;
};


//===========================================================================//

} // namespace Citadel

#endif // LOCATION_NAME_LOGIC_H
