//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef ENEMY_WAVE_H
#define ENEMY_WAVE_H

#include "../../../../BruTech/Source/Helpers/Engine/Engine.h"
#include "EnemyRespawnInfo.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class EnemyWave: public AbstractAction
{
public:
    EnemyWave();
    virtual ~EnemyWave();

    void Add(const SharedPtr<EnemyRespawnInfo> & enemyRespawnInfo);
    void Clear();

    virtual bool Update(float deltaTime);

    const List<SharedPtr<EnemyRespawnInfo>> & GetEnemiesRespawnInfo() const { return _enemiesRespawnInfo; }

private:
    float _currentTime;
    List<SharedPtr<EnemyRespawnInfo>> _enemiesRespawnInfo;

    EnemyWave(const EnemyWave &) = delete;
    EnemyWave & operator =(const EnemyWave &) = delete;
};


//===========================================================================//

} // namespace Citadel

#endif // ENEMY_WAVE_H
