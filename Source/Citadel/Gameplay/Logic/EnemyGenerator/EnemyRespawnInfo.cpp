﻿//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "EnemyRespawnInfo.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

EnemyRespawnInfo::EnemyRespawnInfo(const string enemyType, float spawnTime):
    EnemyType(enemyType),
    SpawnTime(spawnTime)
{
}

//===========================================================================//

} // namespace Citadel
