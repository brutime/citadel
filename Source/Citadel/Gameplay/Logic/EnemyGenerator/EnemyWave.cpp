﻿//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "EnemyWave.h"

#include "../../../Common/EntityManager.h"
#include "../../Gameplay.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

EnemyWave::EnemyWave():
    AbstractAction(Gameplay::EnemyRespawnMask, TaskBlockingState::Blocking, TaskInitializingState::NotReInitializing),
    _currentTime(0.0f),
    _enemiesRespawnInfo()
{
}

//===========================================================================//

EnemyWave::~EnemyWave()
{

}

//===========================================================================//

void EnemyWave::Add(const SharedPtr<EnemyRespawnInfo> & enemyRespawnInfo)
{
    _enemiesRespawnInfo.Add(enemyRespawnInfo);
}

//===========================================================================//

void EnemyWave::Clear()
{
    _enemiesRespawnInfo.Clear();
}

//===========================================================================//

bool EnemyWave::Update(float deltaTime)
{
    if (_enemiesRespawnInfo.IsEmpty())
    {
        return true;
    }

    _currentTime += deltaTime;
    const SharedPtr<EnemyRespawnInfo> & enemyRespawnInfo = _enemiesRespawnInfo.GetFirst();
    if (_currentTime > enemyRespawnInfo->SpawnTime)
    {
        _currentTime = 0.0f;

        SharedPtr<Respawn> respawn = static_pointer_cast<Respawn>(SharedPtr<ActionList>(GetList()));
        const SharedPtr<EnemyInfo> & enemyInfo = respawn->GetEnemyInfo(enemyRespawnInfo->EnemyType);

        SharedPtr<Entity> enemy = EntityManager->CreateEntity(Gameplay::DomainName, respawn->GetGroupName(),
                                  enemyInfo->PathToEntity, enemyInfo->LayerName, respawn->GetPosition());

        _enemiesRespawnInfo.Remove(enemyRespawnInfo);
    }

    return _enemiesRespawnInfo.IsEmpty() ? true : false;
}

//===========================================================================//

} // namespace Citadel
