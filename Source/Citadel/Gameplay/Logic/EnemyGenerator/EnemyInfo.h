//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef ENEMY_INFO_H
#define ENEMY_INFO_H

#include "../../../../BruTech/Source/Helpers/Engine/Engine.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

struct EnemyInfo
{
    EnemyInfo(const string & pathToEntity, const string & layerName);

    string PathToEntity;
    string LayerName;
};


//===========================================================================//

} // namespace Citadel

#endif // ENEMY_INFO_H
