//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef ENEMY_GENERATOR_H
#define ENEMY_GENERATOR_H

#include "../../../../BruTech/Source/Helpers/Engine/Engine.h"
#include "EnemyInfo.h"
#include "EnemyWave.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class Respawn: public ActionList
{
public:
    static const string Type;

    Respawn(const string & domainName, const string & groupName, const Vector3 & position);
    virtual ~Respawn();

    const string & GetGroupName() const { return _groupName; }
    const Vector3 & GetPosition() const { return _position; }

    void AddEnemyInfo(const string & enemyType, const SharedPtr<EnemyInfo> & enemyInfo);
    const SharedPtr<EnemyInfo> & GetEnemyInfo(const string & enemyType) const { return _enemiesInfo[enemyType]; }
    void ClearEnemiesInfo();

private:
    string _groupName;
    Vector3 _position;

    TreeMap<string, SharedPtr<EnemyInfo>> _enemiesInfo;

    Respawn(const Respawn &) = delete;
    Respawn & operator =(const Respawn &) = delete;
};


//===========================================================================//

} // namespace Citadel

#endif // ENEMY_GENERATOR_H
