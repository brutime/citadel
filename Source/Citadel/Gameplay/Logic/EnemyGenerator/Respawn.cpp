﻿//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Respawn.h"

#include "../../../Common/UpdatingOrderTable.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

const string Respawn::Type = "Respawn";

//===========================================================================//

Respawn::Respawn(const string & domainName, const string & groupName, const Vector3 & position):
    ActionList(domainName, UpdatingOrderTable->GetOrder(Type)),
    _groupName(groupName),
    _position(position),
    _enemiesInfo()
{
}

//===========================================================================//

Respawn::~Respawn()
{
}

//===========================================================================//

void Respawn::AddEnemyInfo(const string & enemyType, const SharedPtr<EnemyInfo> & enemyInfo)
{
    _enemiesInfo.Add(enemyType, enemyInfo);
}

//===========================================================================//

} // namespace Citadel
