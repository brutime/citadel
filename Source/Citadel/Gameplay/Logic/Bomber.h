//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef BOMBER_H
#define BOMBER_H

#include "../../../BruTech/Source/Helpers/Engine/Engine.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class Bomber: public AbstractUpdatable
{
public:
    static const string Type;

    Bomber(const string & domainName);
    virtual ~Bomber();

    virtual void Update(float deltaTime);

private:
    void UpdateBombs(float deltaTime);
    void UpdateExplosions();

    Bomber(const Bomber &) = delete;
    Bomber & operator =(const Bomber &) = delete;
};


//===========================================================================//

} // namespace Citadel

#endif // BOMBER_H
