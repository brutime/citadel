﻿//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Bomber.h"

#include "../../Common/EntityManager.h"
#include "../../Common/UpdatingOrderTable.h"
#include "../../Game/Game.h"
#include "../Items/BombLogic.h"

namespace Citadel
{

//===========================================================================//

const string Bomber::Type = "Bomber";

//===========================================================================//

Bomber::Bomber(const string & domainName):
    AbstractUpdatable(domainName, Type, UpdatingOrderTable->GetOrder(Type))
{
}

//===========================================================================//

Bomber::~Bomber()
{
}

//===========================================================================//

void Bomber::Update(float deltaTime)
{
    UpdateBombs(deltaTime);
    UpdateExplosions();
}

//===========================================================================//

void Bomber::UpdateBombs(float deltaTime)
{
    const List<SharedPtr<Entity>> & bombs = EntityManager->GetGroup(Gameplay::BombsGroupName);

    auto it = bombs.CreateIterator();
    while (it.HasNext())
    {
        const SharedPtr<Entity> & bomb = it.GetCurrent();
        it.Next();

        SharedPtr<BombLogic> bombLogic = bomb->GetComponent<BombLogic>(BombLogic::Type);
        SharedPtr<SceneNode> bombNode = bomb->GetComponent<SceneNode>(SceneNode::Type);
        bombNode->TranslateY(-bombLogic->GetFallingSpeed() * deltaTime);

        if (bombNode->GetPosition().Y < Game->GetGameplay()->GetCurrentLocation()->GetLandLineHeight())
        {
            bombLogic->Explose();
            EntityManager->CreateEntity(Gameplay::DomainName, Gameplay::ExplosionsGroupName,
                                        Gameplay::PathToExplosion, Gameplay::GameplayLayerName, bombNode->GetWorldPosition());
            EntityManager->Remove(bomb);
        }
    }
}

//===========================================================================//

void Bomber::UpdateExplosions()
{
    const List<SharedPtr<Entity>> & explosions = EntityManager->GetGroup(Gameplay::ExplosionsGroupName);

    auto it = explosions.CreateIterator();
    while (it.HasNext())
    {
        const SharedPtr<Entity> & explosion = it.GetCurrent();
        it.Next();

        SharedPtr<AnimationController> explosionAnimationController = explosion->GetComponent<AnimationController>(AnimationController::Type);

        if (explosionAnimationController->IsFinished())
        {
            EntityManager->Remove(explosion);
        }
    }
}

//===========================================================================//

} // namespace Citadel
