//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef UNDERTAKER_H
#define UNDERTAKER_H

#include "../../../BruTech/Source/Helpers/Engine/Engine.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class Undertaker: public AbstractUpdatable
{
public:
    static const string Type;

    Undertaker(const string & domainName, const List<string> & dyingGroupsNames);
    virtual ~Undertaker();

    virtual void Update(float);

private:
    List<string> _dyingGroupsNames;

    Undertaker(const Undertaker &) = delete;
    Undertaker & operator =(const Undertaker &) = delete;
};


//===========================================================================//

} // namespace Citadel

#endif // UNDERTAKER_H
