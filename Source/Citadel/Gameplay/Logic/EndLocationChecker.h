//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef END_LOCATION_CHECKER_H
#define END_LOCATION_CHECKER_H

#include "../../../BruTech/Source/Helpers/Engine/Engine.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class EndLocationChecker: public AbstractUpdatable
{
public:
    static const string Type;

    EndLocationChecker(const string & domainName);
    virtual ~EndLocationChecker();

    virtual void Update(float deltaTime);

    integer GetMedalsCount() const;

private:
    static const float PauseAfterWonOrLose;

    bool CheckForFail();
    bool CheckForWin();

    SharedPtr<Entity> GetCitadel() const;

    float _failedTime;
    float _wonTime;

    EndLocationChecker(const EndLocationChecker &) = delete;
    EndLocationChecker & operator =(const EndLocationChecker &) = delete;
};


//===========================================================================//

} // namespace Citadel

#endif // END_LOCATION_CHECKER_H
