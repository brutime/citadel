﻿//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "EndLocationChecker.h"

#include "../../Common/EntityManager.h"
#include "../../Common/UpdatingOrderTable.h"
#include "../../Game/Game.h"
#include "../Gameplay.h"
#include "../CommonComponents/Health.h"

namespace Citadel
{

//===========================================================================//

const string EndLocationChecker::Type = "EndLocationChecker";

const float EndLocationChecker::PauseAfterWonOrLose = 1.5f;

//===========================================================================//

EndLocationChecker::EndLocationChecker(const string & domainName):
    AbstractUpdatable(domainName, Type, UpdatingOrderTable->GetOrder(Type)),
    _failedTime(0.0f),
    _wonTime(0.0f)
{
}

//===========================================================================//

EndLocationChecker::~EndLocationChecker()
{
}

//===========================================================================//

void EndLocationChecker::Update(float deltaTime)
{
    if (CheckForFail())
    {
        _failedTime += deltaTime;
        if (_failedTime > PauseAfterWonOrLose)
        {
            Game->AddStateToQueue(GameState::GameOver);
            _failedTime = 0.0f;
        }
    }
    else if (CheckForWin())
    {
        _wonTime += deltaTime;
        if (_wonTime > PauseAfterWonOrLose)
        {
            Game->AddStateToQueue(GameState::LocationComplete);
            _wonTime = 0.0f;
        }
    }
}

//===========================================================================//

integer EndLocationChecker::GetMedalsCount() const
{
    SharedPtr<Entity> citadel = GetCitadel();
    if (citadel == NullPtr)
    {
        return 0;
    }

    SharedPtr<Health> citadelHealth = citadel->GetComponent<Health>(Health::Type);
    if (citadelHealth->GetPercentage() > 90)
    {
        return 3;
    }
    else if (citadelHealth->GetPercentage() > 60)
    {
        return 2;
    }
    else if (citadelHealth->GetPercentage() > 35)
    {
        return 1;
    }

    return 0;
}

//===========================================================================//

bool EndLocationChecker::CheckForWin()
{
    return Game->GetGameplay()->GetCurrentLocation()->IsRespawnsEmpty() &&
           EntityManager->IsGroupEmpty(Gameplay::EnemiesGroupName);
}

//===========================================================================//

bool EndLocationChecker::CheckForFail()
{
    return GetCitadel() == NullPtr;
}

//===========================================================================//

SharedPtr<Entity> EndLocationChecker::GetCitadel() const
{
    const List<SharedPtr<Entity>> & buildings = EntityManager->GetGroup(Gameplay::BuildingsGroupName);
    for (const SharedPtr<Entity> & building : buildings)
    {
        if (building->GetType() == "Citadel")
        {
            return building;
        }
    }

    return NullPtr;
}

//===========================================================================//

} // namespace Citadel
