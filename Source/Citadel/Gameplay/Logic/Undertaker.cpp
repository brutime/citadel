﻿//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Undertaker.h"

#include "../../Common/EntityManager.h"
#include "../../Common/UpdatingOrderTable.h"
#include "../CommonComponents/Health.h"

namespace Citadel
{

//===========================================================================//

const string Undertaker::Type = "Undertaker";

//===========================================================================//

Undertaker::Undertaker(const string & domainName, const List<string> & dyingGroupsNames):
    AbstractUpdatable(domainName, Type, UpdatingOrderTable->GetOrder(Type)),
    _dyingGroupsNames(dyingGroupsNames)
{
}

//===========================================================================//

Undertaker::~Undertaker()
{
}

//===========================================================================//

void Undertaker::Update(float)
{
    for (const string & groupName : _dyingGroupsNames)
    {
        const List<SharedPtr<Entity>> & entities = EntityManager->GetGroup(groupName);

        auto it = entities.CreateIterator();
        while (it.HasNext())
        {
            const SharedPtr<Entity> & entity = it.GetCurrent();
            it.Next();

            SharedPtr<Health> health = entity->GetComponent<Health>(Health::Type);
            if (health->IsDead())
            {
                EntityManager->Remove(entity);
            }
        }
    }
}

//===========================================================================//

} // namespace Citadel
