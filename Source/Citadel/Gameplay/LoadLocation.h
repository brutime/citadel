//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#ifndef LOAD_LOCATION_H
#define LOAD_LOCATION_H

#include "../../BruTech/Source/Helpers/Engine/Engine.h"

#include "../Game/IGameState.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class LoadLocation: public IGameState
{
public:
    static const string Name;

    LoadLocation();
    virtual ~LoadLocation();

    virtual void Select();

    void SetLocationName(const string & locationName) { _locationName = locationName; }

private:
    string _locationName;

    LoadLocation(const LoadLocation &) = delete;
    LoadLocation & operator =(const LoadLocation &) = delete;
};

//===========================================================================//

} // namespace Citadel

#endif // LOAD_LOCATION_H
