//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "GameOver.h"

#include "../Game/Game.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

const string GameOver::Name = "Game over state";

//===========================================================================//

GameOver::GameOver(): IGameState()
{
    Console->Notice(string::Format("{0} initializing...", Name));
    Console->AddOffset(2);

    Console->ReduceOffset(2);
    Console->Notice(string::Format("{0} initialized", Name));
}

//===========================================================================//

GameOver::~GameOver()
{
    Console->Notice(string::Format("{0} shutting down...", Name));
    Console->AddOffset(2);

    Console->ReduceOffset(2);
    Console->Notice(string::Format("{0} has shut down", Name));
}

//===========================================================================//

void GameOver::Select()
{
    Console->Notice(string::Format("{0} selected", Name));

    Game->AddStateToQueue(GameState::Gameplay);
    Game->GetGameplay()->GameOver();
}

//===========================================================================//

} // namespace Citadel
