//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "NextLocation.h"

#include "../Game/Game.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

const string NextLocation::Name = "Next location state";

//===========================================================================//

NextLocation::NextLocation(): IGameState()
{
    Console->Notice(string::Format("{0} initializing...", Name));
    Console->AddOffset(2);

    Console->ReduceOffset(2);
    Console->Notice(string::Format("{0} initialized", Name));
}

//===========================================================================//

NextLocation::~NextLocation()
{
    Console->Notice(string::Format("{0} shutting down...", Name));
    Console->AddOffset(2);

    Console->ReduceOffset(2);
    Console->Notice(string::Format("{0} has shut down", Name));
}

//===========================================================================//

void NextLocation::Select()
{
    Console->Notice(string::Format("{0} selected", Name));

    Game->AddStateToQueue(GameState::Gameplay);
    Game->GetGameplay()->NextLocation();
}

//===========================================================================//

} // namespace Citadel
