//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс отвечает за реализацию геймплея
//

#ifndef LOCATION_COMPLETE_H
#define LOCATION_COMPLETE_H

#include "../../BruTech/Source/Helpers/Engine/Engine.h"

#include "../Game/IGameState.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class LocationComplete: public IGameState
{
public:
    static const string Name;

    LocationComplete();
    virtual ~LocationComplete();

    virtual void Select();

private:
    LocationComplete(const LocationComplete &) = delete;
    LocationComplete & operator =(const LocationComplete &) = delete;
};

//===========================================================================//

} // namespace Citadel

#endif // LOCATION_COMPLETE_H
