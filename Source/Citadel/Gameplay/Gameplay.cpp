//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//


#include "Gameplay.h"

#include "../CitadelApplication.h"
#include "../Common/EntityManager.h"
#include "../Game/Game.h"
#include "Units/Balloon/BalloonInputReceiver.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

const string Gameplay::Name = "Gameplay state";
const string Gameplay::DomainName = "Gameplay";
const string Gameplay::GameplayLayerName = "Gameplay";
const string Gameplay::HUDLayerName = "HUD";
const string Gameplay::GUILayerName = "GUI";

const string Gameplay::DecorationsGroupName = "Decorations";
const string Gameplay::BuildingsGroupName = "Buildings";
const string Gameplay::EnemiesGroupName = "Enemies";
const string Gameplay::BombsGroupName = "Bombs";
const string Gameplay::ExplosionsGroupName = "Explosions";
const string Gameplay::PlayersGroupName = "Players";
const string Gameplay::HUDGroupName = "HUD";

const string Gameplay::PathToLocations = "Locations";
const string Gameplay::PathToPathToLocationKey = "PathToLocation";

const string Gameplay::BehaviorMask = "Move";
const string Gameplay::EnemyRespawnMask = "EnemyRespawnMask";

const float Gameplay::MovementAccuracy = 0.05f;

const string Gameplay::PathToBomb = "Items/Bomb";
const string Gameplay::PathToExplosion = "Items/Explosion";

//===========================================================================//

Gameplay::Gameplay(): IGameState(),
    _locationCompleteMenu(),
    _gameOverMenu(),
    _bomber(),
    _undertaker(),
    _endLocationChecker(),
    _locationNames(),
    _currentLocation(),
    _inputReceiver(),
    _camera()
{
    Console->Notice(string::Format("{0} initializing...", Name));
    Console->AddOffset(2);

    DomainManager->Add(DomainName);

    Renderer->AddLayerToFront(DomainName, new RenderableLayer(HUDLayerName));
    Renderer->AddLayerToFront(DomainName, new RenderableLayer(GUILayerName, ProjectionMode::Ortho));

    EntityManager->AddGroup(DecorationsGroupName);
    EntityManager->AddGroup(BuildingsGroupName);
    EntityManager->AddGroup(EnemiesGroupName);
    EntityManager->AddGroup(BombsGroupName);
    EntityManager->AddGroup(ExplosionsGroupName);
    EntityManager->AddGroup(PlayersGroupName);
    EntityManager->AddGroup(HUDGroupName);

    LoadLocationNames();

    _locationCompleteMenu = new LocationCompleteMenu(DomainName, GUILayerName);
    _locationCompleteMenu->Hide();

    _gameOverMenu = new GameOverMenu(DomainName, GUILayerName);
    _gameOverMenu->Hide();

    _bomber = new Bomber(DomainName);
    _undertaker = new Undertaker(DomainName, { BuildingsGroupName, EnemiesGroupName });
    _endLocationChecker = new EndLocationChecker(DomainName);

    _inputReceiver = new GameplayInputReceiver(DomainName);

    _camera = new Camera();
    _camera->Set2DPosition(CitadelApplication::SurfaceCoordsWidth / 2.0f,
                           CitadelApplication::SurfaceCoordsHeight / 2.0f,
                           CitadelApplication::SurfaceCoordsHeight / 2.0f);

    RegisterCommands();

    Console->ReduceOffset(2);
    Console->Notice(string::Format("{0} initialized", Name));
}

//===========================================================================//

Gameplay::~Gameplay()
{
    Console->Notice(string::Format("{0} shutting down...", Name));
    Console->AddOffset(2);

    UnregisterCommands();

    _camera = NullPtr;

    _inputReceiver = NullPtr;

    _bomber = NullPtr;
    _undertaker = NullPtr;
    _endLocationChecker = NullPtr;

    _currentLocation = NullPtr;

    _gameOverMenu = NullPtr;
    _locationCompleteMenu = NullPtr;

    EntityManager->RemoveGroup(DecorationsGroupName);
    EntityManager->RemoveGroup(BuildingsGroupName);
    EntityManager->RemoveGroup(EnemiesGroupName);
    EntityManager->RemoveGroup(BombsGroupName);
    EntityManager->RemoveGroup(ExplosionsGroupName);
    EntityManager->RemoveGroup(PlayersGroupName);
    EntityManager->RemoveGroup(HUDGroupName);

    DomainManager->Remove(DomainName);

    Console->ReduceOffset(2);
    Console->Notice(string::Format("{0} has shut down", Name));
}

//===========================================================================//

void Gameplay::Select()
{
    Console->Notice(string::Format("{0} selected", Name));

    _inputReceiver->Enable();
    Scene->ApplyCamera(_camera);
    if (!_gameOverMenu->IsVisible() && !_locationCompleteMenu->IsVisible())
    {
        GUIManager->HideCursor();
    }
    DomainManager->Select(DomainName);
}

//===========================================================================//

void Gameplay::Leave()
{
    _inputReceiver->Disable();
}

//===========================================================================//

bool Gameplay::IsActive() const
{
    return _currentLocation != NullPtr;
}

//===========================================================================//

void Gameplay::NewGame()
{
    if (_locationNames.GetCount() != 0)
    {
        LoadLocation(_locationNames[0]);
    }
}

//===========================================================================//

void Gameplay::LoadLocation(const string & pathToLocation)
{
    if (_currentLocation != NullPtr)
    {
        _currentLocation = NullPtr;
    }

    _currentLocation = new Location(DomainName, pathToLocation);

    _endLocationChecker->Enable();

    EnablePlayersInput();

    _locationCompleteMenu->Hide();
    _gameOverMenu->Hide();
    GUIManager->HideCursor();

    Engine->ResetTiming();
}

//===========================================================================//

void Gameplay::NextLocation()
{
    if (!HasNextLocation())
    {
        INVALID_STATE_EXCEPTION("No next location");
    }

    integer locationIndex = GetIndexOfLocationName(_currentLocation->GetPathToFile());
    LoadLocation(_locationNames[locationIndex + 1]);
}

//===========================================================================//

void Gameplay::LocationComplete()
{
    _endLocationChecker->Disable();

    DisablePlayersInput();

    _locationCompleteMenu->SetShowingMedalsCount(_endLocationChecker->GetMedalsCount());
    _locationCompleteMenu->Show();
    GUIManager->ShowCursor();
}

//===========================================================================//

void Gameplay::GameOver()
{
    _currentLocation->DisableRespawns();
    _endLocationChecker->Disable();

    DisablePlayersInput();

    _gameOverMenu->Show();
    GUIManager->ShowCursor();
}

//===========================================================================//

void Gameplay::DestroyCurrentLocation()
{
    _currentLocation = NullPtr;
}

//===========================================================================//

bool Gameplay::HasNextLocation()
{
    return GetIndexOfLocationName(_currentLocation->GetPathToFile()) < _locationNames.GetCount() - 1;
}

//===========================================================================//

void Gameplay::LoadLocationNames()
{
    SharedPtr<ParametersFile> locationListFile = new ParametersFile(PathHelper::PathToLocations + "List");

    integer locationsCount = locationListFile->GetListChildren(PathToLocations);

    _locationNames.Resize(locationsCount);
    for (integer i = 0; i < locationsCount; i++)
    {
        string pathToLocation = string::Format(PathToLocations + ".{0}." + PathToPathToLocationKey, i);
        _locationNames[i] = locationListFile->GetString(pathToLocation);
    }
}

//===========================================================================//

void Gameplay::EnablePlayersInput()
{
    const List<SharedPtr<Entity>> & players = EntityManager->GetGroup(Gameplay::PlayersGroupName);
    for (const SharedPtr<Entity> & player : players)
    {
        SharedPtr<BalloonInputReceiver> inputReceiver = player->GetComponent<BalloonInputReceiver>(BalloonInputReceiver::Type);
        inputReceiver->Enable();
    }
}

//===========================================================================//

void Gameplay::DisablePlayersInput()
{
    const List<SharedPtr<Entity>> & players = EntityManager->GetGroup(Gameplay::PlayersGroupName);
    for (const SharedPtr<Entity> & player : players)
    {
        SharedPtr<BalloonInputReceiver> inputReceiver = player->GetComponent<BalloonInputReceiver>(BalloonInputReceiver::Type);
        inputReceiver->Disable();
    }
}

//===========================================================================//

integer Gameplay::GetIndexOfLocationName(const string & locationName) const
{
    //Не пользуемся IndexOf чтобы сравнивать без учета регистра
    for (integer i = 0; i < _locationNames.GetCount(); i++)
    {
        if (locationName.ToUpper() == _locationNames[i].ToUpper())
        {
            return i;
        }
    }

    INVALID_STATE_EXCEPTION(string::Format("Location with name \"{0}\" not found", locationName));
}

//===========================================================================//

void Gameplay::PrintWavesInfo() const
{
    for (const SharedPtr<Respawn> & respawn : _currentLocation->GetRespawns())
    {
        Console->Notice(string::Format("Group name: \"{0}\"", respawn->GetGroupName()));
        Console->Notice(string::Format("Position: {0}", respawn->GetPosition().ToString()));
        Console->Notice("Waves:");
        Console->AddOffset(2);
        auto it = respawn->GetActions().CreateIterator();
        for (it.First(); it.HasNext(); it.Next())
        {
            SharedPtr<EnemyWave> wave = static_pointer_cast<EnemyWave>(it.GetCurrent());

            Console->Notice(string::Format("Count: {0}", wave->GetEnemiesRespawnInfo().GetCount()));
            Console->AddOffset(2);
            for (const SharedPtr<EnemyRespawnInfo> & enemyRespawnInfo : wave->GetEnemiesRespawnInfo())
            {
                Console->Notice(string::Format("Enemy type: \"{0}\"", enemyRespawnInfo->EnemyType));
                Console->Notice(string::Format("Spawn time: {0}", enemyRespawnInfo->SpawnTime));
            }
            Console->ReduceOffset(2);
        }
        Console->ReduceOffset(2);
    }
}

//===========================================================================//

void Gameplay::LoadLocationCommand(const string & pathToLocation)
{
    bool found = false;
    //Не пользуемся IndexOf чтобы сравнивать без учета регистра
    for (const string & locationName : _locationNames)
    {
        if (pathToLocation.ToUpper() == locationName.ToUpper())
        {
            found = true;
            break;
        }
    }

    if (!found)
    {
        Console->Error(string::Format("Location \"{0}\" not found", pathToLocation));
        return;
    }

    Game->GetLoadLocation()->SetLocationName(pathToLocation);
    Game->AddStateToQueue(GameState::LoadLocation);
}

//===========================================================================//

void Gameplay::RegisterCommands()
{
    ScriptSystem->RegisterCommand(new ClassConstCommand<Gameplay, void>("Waves", this, &Gameplay::PrintWavesInfo));
    ScriptSystem->RegisterCommand(new ClassCommand<Gameplay, void, const string &>("LoadLocation", this, &Gameplay::LoadLocationCommand));
}

//===========================================================================//

void Gameplay::UnregisterCommands()
{
    ScriptSystem->UnregisterCommand("LoadLocation");
    ScriptSystem->UnregisterCommand("Waves");
}

//===========================================================================//

} // namespace Citadel
