﻿//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Location.h"

#include "../CitadelApplication.h"
#include "../Common/EntityManager.h"
#include "Gameplay.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

const string Location::NameKey = "Name";
const string Location::WidthKey = "Width";
const string Location::HeightKey = "Height";
const string Location::LandLineHeightKey = "LandLineHeight";
const string Location::BalloonBoundsLeftKey = "BalloonBoundsLeft";
const string Location::BalloonBoundsRightKey = "BalloonBoundsRight";

const string Location::PathToTextures = "Textures";
const string Location::PathToPathToTexture = "PathToTexture";

const string Location::PathParametersFiles = "ParametersFiles";
const string Location::PathToPathParameterFiles = "PathToParametersFile";

const string Location::PathToLayers = "Layers";

const string Location::PathToCachedEntities = "CachedEntities";
const string Location::PathToEntities = "Entities";

const string Location::PathToRespawns = "Respawns";
const string Location::PathToEnemiesInfo = "EnemiesInfo";
const string Location::PathToWaves = "Waves";

const string Location::LayerNameKey = "Name";
const string Location::GroupNameKey = "GroupName";
const string Location::EntityPathToEntityKey = "PathToEntity";
const string Location::EntityLayerNameKey = "LayerName";
const string Location::EntityPositionKey = "Position";
const string Location::EntityCountKey = "Count";
const string Location::RespawnPositionKey = "Position";
const string Location::EnemyInfoTypeKey = "Type";
const string Location::EnemyInfoPathToEntityKey = "PathToEntity";
const string Location::EnemyInfoLayerNameKey = "LayerName";
const string Location::EnemyTypeKey = "EnemyType";
const string Location::EnemySpawnTimeKey = "SpawnTime";

//===========================================================================//

Location::Location(const string & domainName, const string & pathToFile):
    _domainName(domainName),
    _pathToFile(pathToFile),
    _fullPathToFile(PathHelper::GetFullPathToLocationFile(pathToFile)),
    _name(),
    _width(0.0f),
    _height(0.0f),
    _landLineHeight(0.0f),
    _balloonBoundsLeft(0.0f),
    _balloonBoundsRight(0.0f),
    _respawns(),
    _locationName()
{
    Console->Notice(string::Format("Loading location \"{0}\" ...", _fullPathToFile));
    Console->AddOffset(2);

    ParametersFileManager->Load(_fullPathToFile);

    Load();

    Console->ReduceOffset(2);
    Console->Notice("Location loaded");
}

//===========================================================================//

Location::~Location()
{
    Console->Notice(string::Format("Removing location \"{0}\" ...", _fullPathToFile));
    Console->AddOffset(2);

    Clear();

    ParametersFileManager->Remove(_fullPathToFile);

    Console->ReduceOffset(2);
    Console->Notice("Location removed");
}

//===========================================================================//

void Location::EnableRespawns()
{
    for (const SharedPtr<Respawn> & respawn : _respawns)
    {
        respawn->Enable();
    }
}

//===========================================================================//

void Location::DisableRespawns()
{
    for (const SharedPtr<Respawn> & respawn : _respawns)
    {
        respawn->Disable();
    }
}

//===========================================================================//

bool Location::IsRespawnsEmpty()
{
    for (const SharedPtr<Respawn> & respawn : _respawns)
    {
        if (!respawn->IsEmpty())
        {
            return false;
        }
    }

    return true;
}

//===========================================================================//

void Location::Load()
{
    WeakPtr<ParametersFile> file = ParametersFileManager->Get(_fullPathToFile);

    _name = StringTable->GetString(file->GetString(NameKey));
    Console->Notice(string::Format("Location name: \"{0}\"", _name));

    _width = file->GetFloat(NameKey);
    _height = file->GetFloat(NameKey);

    _landLineHeight = file->GetFloat(LandLineHeightKey);

    _balloonBoundsLeft = file->GetFloat(BalloonBoundsLeftKey);
    _balloonBoundsRight = file->GetFloat(BalloonBoundsRightKey);

    LoadData();

    //Layers
    integer layersCount = file->GetListChildren(PathToLayers);
    for (integer i = 0; i < layersCount; i++)
    {
        string layerName = file->GetString(string::Format(PathToLayers + ".{0}." + LayerNameKey, i));
        Renderer->AddLayerToBack(_domainName, new RenderableLayer(layerName));
    }

    LoadCachedEntities();
    LoadEntities();
    LoadRespawns();
    SetupLocationName();
}

//===========================================================================//

void Location::Clear()
{
    EntityManager->ClearGroup(Gameplay::DecorationsGroupName);
    EntityManager->ClearGroup(Gameplay::BuildingsGroupName);
    EntityManager->ClearGroup(Gameplay::EnemiesGroupName);
    EntityManager->ClearGroup(Gameplay::BombsGroupName);
    EntityManager->ClearGroup(Gameplay::ExplosionsGroupName);
    EntityManager->ClearGroup(Gameplay::PlayersGroupName);
    EntityManager->ClearGroup(Gameplay::HUDGroupName);

    _locationName = NullPtr;

    _respawns.Clear();

    WeakPtr<ParametersFile> file = ParametersFileManager->Get(_fullPathToFile);

    //Layers
    integer layersCount = file->GetListChildren(PathToLayers);
    for (integer i = 0; i < layersCount; i++)
    {
        string layerName = file->GetString(string::Format(PathToLayers + ".{0}." + LayerNameKey, i));
        Renderer->RemoveLayer(_domainName, layerName);
    }

    RemoveData();
}

//===========================================================================//

void Location::LoadData()
{
    WeakPtr<ParametersFile> file = ParametersFileManager->Get(_fullPathToFile);

    integer texturesCount = file->GetListChildren(PathToTextures);
    for (integer i = 0; i < texturesCount; i++)
    {
        string parameterPath = string::Format(PathToTextures + ".{0}." + PathToPathToTexture, i);
        string pathToTexture = file->GetString(parameterPath);
        TextureManager->Load(pathToTexture);
    }

    integer parametersFilesCount = file->GetListChildren(PathParametersFiles);
    for (integer i = 0; i < parametersFilesCount; i++)
    {
        string parameterPath = string::Format(PathParametersFiles + ".{0}." + PathToPathParameterFiles, i);
        string pathToParametersFile = file->GetString(parameterPath);
        ParametersFileManager->Load(pathToParametersFile);
    }
}

//===========================================================================//

void Location::RemoveData()
{
    WeakPtr<ParametersFile> file = ParametersFileManager->Get(_fullPathToFile);

    integer texturesCount = file->GetListChildren(PathToTextures);
    for (integer i = 0; i < texturesCount; i++)
    {
        string parameterPath = string::Format(PathToTextures + ".{0}." + PathToPathToTexture, i);
        string pathToTexture = file->GetString(parameterPath);
        TextureManager->Remove(pathToTexture);
    }

    integer parametersFilesCount = file->GetListChildren(PathParametersFiles);
    for (integer i = 0; i < parametersFilesCount; i++)
    {
        string parameterPath = string::Format(PathParametersFiles + ".{0}." + PathToPathParameterFiles, i);
        string pathToParametersFile = file->GetString(parameterPath);
        ParametersFileManager->Remove(pathToParametersFile);
    }
}

//===========================================================================//

void Location::LoadCachedEntities()
{
    WeakPtr<ParametersFile> file = ParametersFileManager->Get(_fullPathToFile);

    integer groupsCount = file->GetListChildren(PathToCachedEntities);

    for (integer i = 0; i < groupsCount; i++)
    {
        string path = string::Format("{0}.{1}.", PathToCachedEntities, i);
        string groupName = file->GetString(path + GroupNameKey);
        integer entitiesCount = file->GetListChildren(path);
        for (integer j = 0; j < entitiesCount; j++)
        {
            string pathToEntity = file->GetString(string::Format(path + "{0}." + EntityPathToEntityKey, j));
            string layerName = file->GetString(string::Format(path + "{0}." + EntityLayerNameKey, j));
            integer count = file->GetInteger(string::Format(path + "{0}." + EntityCountKey, j));

            EntityManager->ReserveEntities(_domainName, groupName, pathToEntity, layerName, Vector3(), count);
        }
    }
}

//===========================================================================//

void Location::LoadEntities()
{
    WeakPtr<ParametersFile> file = ParametersFileManager->Get(_fullPathToFile);

    integer groupsCount = file->GetListChildren(PathToEntities);

    for (integer i = 0; i < groupsCount; i++)
    {
        string path = string::Format("{0}.{1}.", PathToEntities, i);
        string groupName = file->GetString(path + GroupNameKey);
        integer entitiesCount = file->GetListChildren(path);
        for (integer j = 0; j < entitiesCount; j++)
        {
            string pathToEntity = file->GetString(string::Format(path + "{0}." + EntityPathToEntityKey, j));
            string layerName = file->GetString(string::Format(path + "{0}." + EntityLayerNameKey, j));
            Vector3 position = file->GetCustom<Vector3>(string::Format(path + "{0}." + EntityPositionKey, j));

            EntityManager->CreateEntity(_domainName, groupName, pathToEntity, layerName, position);
        }
    }
}

//===========================================================================//

void Location::LoadRespawns()
{
    WeakPtr<ParametersFile> file = ParametersFileManager->Get(_fullPathToFile);

    integer respawnsCount = file->GetListChildren(PathToRespawns);

    for (integer i = 0; i < respawnsCount; i++)
    {
        string respawnPath = string::Format("{0}.{1}.", PathToRespawns, i);
        string groupName = file->GetString(respawnPath + GroupNameKey);
        Vector3 position = file->GetCustom<Vector3>(respawnPath + RespawnPositionKey);

        SharedPtr<Respawn> respawn = new Respawn(_domainName, groupName, position);

        integer enemiesInfoCount = file->GetListChildren(respawnPath + PathToEnemiesInfo);
        for (integer j = 0; j < enemiesInfoCount; j++)
        {
            string enemyInfoPath = string::Format("{0}.{1}.", respawnPath + PathToEnemiesInfo, j);
            string type = file->GetString(enemyInfoPath + EnemyInfoTypeKey);
            string pathToEntity = file->GetString(enemyInfoPath + EnemyInfoPathToEntityKey);
            string layerName = file->GetString(enemyInfoPath + EnemyInfoLayerNameKey);

            respawn->AddEnemyInfo(type, new EnemyInfo(pathToEntity, layerName));
        }

        integer wavesCount = file->GetListChildren(respawnPath + PathToWaves);
        for (integer k = 0; k < wavesCount; k++)
        {
            string wavesPath = string::Format("{0}.{1}", respawnPath + PathToWaves, k);

            SharedPtr<EnemyWave> wave = new EnemyWave();

            integer enemiesRespawnInfoCount = file->GetListChildren(wavesPath);
            for (integer l = 0; l < enemiesRespawnInfoCount; l++)
            {
                string wavePath = string::Format("{0}.{1}.", wavesPath, l);
                string enemyType = file->GetString(wavePath + EnemyTypeKey);
                float spawnTime = file->GetFloat(wavePath + EnemySpawnTimeKey);

                wave->Add(new EnemyRespawnInfo(enemyType, spawnTime));
            }

            respawn->Add(wave);
        }

        _respawns.Add(respawn);
    }
}

//===========================================================================//

void Location::SetupLocationName()
{
    _locationName = EntityManager->CreateEntity(_domainName, Gameplay::HUDGroupName, "HUD/LocationName",
                    Gameplay::GUILayerName, Vector3());

    SharedPtr<RenderableText> renderableText = _locationName->GetComponent<RenderableText>(RenderableText::Type);
    renderableText->SetText(_name);

    SharedPtr<SceneNode> locationNameNode = _locationName->GetComponent<SceneNode>(SceneNode::Type);
    locationNameNode->SetPosition(0.5f - renderableText->GetWidth() / 2.0f, 0.7f, 0.0f);
}

//===========================================================================//

} // namespace Citadel
