//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "EntityManager.h"

#include "../Game/Game.h"
#include "../Gameplay/Gameplay.h"
#include "../Intro/WidgetsComponents/LogoLogic.h"
#include "../Gameplay/CommonComponents/Attack.h"
#include "../Gameplay/CommonComponents/Flipper.h"
#include "../Gameplay/CommonComponents/Health.h"
#include "../Gameplay/Items/BombLogic.h"
#include "../Gameplay/HUDComponents/HealthBarLogic.h"
#include "../Gameplay/HUDComponents/LocationNameLogic.h"
#include "../Gameplay/Units/Balloon/BalloonLogic.h"
#include "../Gameplay/Units/Balloon/BalloonInputReceiver.h"
#include "../Gameplay/Units/Warrior/WarriorLogic.h"
#include "UpdatingOrderTable.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

SharedPtr<EntityManagerSingleton> EntityManager;

//===========================================================================//

const string EntityManagerSingleton::Name = "Entity manager";
const string EntityManagerSingleton::TypeKey = "Type";
const string EntityManagerSingleton::PathToComponents = "Components";
const string EntityManagerSingleton::ComponentTypeKey = "Type";
const string EntityManagerSingleton::PathToSpriteKey = "PathToSprite";
const string EntityManagerSingleton::PathToRenderableTextKey = "PathToRenderableText";
const string EntityManagerSingleton::InertiaSlowdownFactorKey = "InertiaSlowdownFactor";
const string EntityManagerSingleton::SpeedKey = "Speed";
const string EntityManagerSingleton::MaxHealthKey = "MaxHealth";
const string EntityManagerSingleton::PowerKey = "Power";
const string EntityManagerSingleton::RadiusKey = "Radius";
const string EntityManagerSingleton::PrepareTimeKey = "PrepareTime";
const string EntityManagerSingleton::FallingSpeedKey = "FallingSpeed";
const string EntityManagerSingleton::HealthBarYKey = "HealthBarY";
const string EntityManagerSingleton::YellowZonePercentKey = "YellowZonePercent";
const string EntityManagerSingleton::RedZonePercentKey = "RedZonePercent";
const string EntityManagerSingleton::GreenZoneColorKey = "GreenZoneColor";
const string EntityManagerSingleton::YellowZoneColorKey = "YellowZoneColor";
const string EntityManagerSingleton::RedZoneColorKey = "RedZoneColor";
const string EntityManagerSingleton::AlphaEnhanceSpeedKey = "AlphaEnhanceSpeed";
const string EntityManagerSingleton::ShowingTimeKey = "ShowingTime";
const string EntityManagerSingleton::DeltaWidthKey = "DeltaWidth";
const string EntityManagerSingleton::DeltaHeightKey = "DeltaHeight";

const string EntityManagerSingleton::PathToHealthBarBackgroundEntity = "HUD/HealthBarBackground";
const string EntityManagerSingleton::PathToHealthBarEntity = "HUD/HealthBar";

//===========================================================================//

EntityManagerSingleton::EntityManagerSingleton():
    _entities(),
    _cachedEntities()
{
    Console->Notice(string::Format("{0} initializing...", Name));
    Console->AddOffset(2);

    RegisterCommands();

    Console->ReduceOffset(2);
    Console->Notice(string::Format("{0} initialized", Name));
}

//===========================================================================//

EntityManagerSingleton::~EntityManagerSingleton()
{
    Console->Notice(string::Format("{0} shutting down...", Name));
    Console->AddOffset(2);

    UnregisterCommands();

    Clear();

    Console->ReduceOffset(2);
    Console->Notice(string::Format("{0} has shut down", Name));
}

//===========================================================================//

SharedPtr<Entity> EntityManagerSingleton::CreateEntity(const string & domainName, const string & groupName,
        const string & pathToEntity, const string & layerName, const Vector3 & position)
{
    WeakPtr<ParametersFile> file = ParametersFileManager->Get(PathHelper::GetFullPathToEntityFile(pathToEntity));
    string entityType = file->GetString(TypeKey);

    SharedPtr<Entity> entity;
    if (_cachedEntities[groupName].Contains(entityType) && _cachedEntities[groupName][entityType].GetCount() > 0)
    {
        entity = ReturnCachedEntity(groupName, entityType, layerName, position);
        AddWithChildren(entity);
        RemoveWithChildrenFromCache(entity);
    }
    else
    {
        entity = CreateNewEntity(domainName, groupName, pathToEntity, layerName, position);
        AddWithChildren(entity);
    }

    return entity;
}

//===========================================================================//

void EntityManagerSingleton::ReserveEntities(const string & domainName, const string & groupName,
        const string & pathToEntity, const string & layerName, const Vector3 & position, integer count)
{
    for (integer i = 0; i < count; i++)
    {
        SharedPtr<Entity> entity = CreateNewEntity(domainName, groupName, pathToEntity, layerName, position);
        entity->PutToCache();
        AddWithChildrenToCache(entity);
    }
}

//===========================================================================//

bool EntityManagerSingleton::Contains(const SharedPtr<Entity> & entity)
{
    if (_entities.Contains(entity->GetGroupName()))
    {
        return _entities[entity->GetGroupName()].Contains(entity);
    }

    return false;
}

//===========================================================================//

void EntityManagerSingleton::Remove(const SharedPtr<Entity> & entity)
{
#ifdef DEBUGGING
    if (entity->GetParent() != NullPtr)
    {
        INVALID_ARGUMENT_EXCEPTION(string::Format("Entity \"{0}\" has parent \"{1}\"",
                                   entity->GetName(), entity->GetParent()->GetName()));
    }
#endif

    entity->PutToCache();
    AddWithChildrenToCache(entity);
    RemoveWithChildren(entity);
}

//===========================================================================//

void EntityManagerSingleton::AddGroup(const string & groupName)
{
    _entities.Add(groupName, List<SharedPtr<Entity>>());
    _cachedEntities.Add(groupName, TreeMap<string, List<SharedPtr<Entity>>>());
}

//===========================================================================//

const List<SharedPtr<Entity>> & EntityManagerSingleton::GetGroup(const string & groupName) const
{
    return _entities[groupName];
}

//===========================================================================//

bool EntityManagerSingleton::IsGroupEmpty(const string & groupName)
{
    return _entities[groupName].IsEmpty();
}

//===========================================================================//

void EntityManagerSingleton::ClearGroup(const string & groupName)
{
    _entities[groupName].Clear();
    _cachedEntities[groupName].Clear();
}

//===========================================================================//

void EntityManagerSingleton::RemoveGroup(const string & groupName)
{
    _entities.Remove(groupName);
    _cachedEntities.Remove(groupName);
}

//===========================================================================//

void EntityManagerSingleton::Clear()
{
    _entities.Clear();
    _cachedEntities.Clear();
}

//===========================================================================//

integer EntityManagerSingleton::GetCount(const string & groupName) const
{
    return _entities[groupName].GetCount();
}

//===========================================================================//

integer EntityManagerSingleton::GetCount() const
{
    integer totalCount = 0;
    for (auto & pair : _entities)
    {
        totalCount += pair.GetValue().GetCount();
    }
    return totalCount;
}

//===========================================================================//

SharedPtr<Entity> EntityManagerSingleton::CreateNewEntity(const string & domainName, const string & groupName,
        const string & pathToEntity, const string & layerName,
        const Vector3 & position)
{
    WeakPtr<ParametersFile> file = ParametersFileManager->Get(PathHelper::GetFullPathToEntityFile(pathToEntity));

    string entityType = file->GetString(TypeKey);

    SharedPtr<Entity> entity = new Entity(groupName, entityType);

    //Components
    integer componentsCount = file->GetListChildren(PathToComponents);
    for (integer i = 0; i < componentsCount; i++)
    {
        string componentType = file->GetString(string::Format(PathToComponents + ".{0}." + ComponentTypeKey, i));
        if (componentType == SceneNode::Type)
        {
            AddSceneNodeComponent(domainName, entity, position);
        }
        else if (componentType == Sprite::Name)
        {
            AddSpriteComponent(file, domainName, entity, layerName, i);
        }
        else if (componentType == AnimatedSprite::Name)
        {
            AddAnimatedSpriteComponent(file, domainName, entity, layerName, i);
        }
        else if (componentType == RenderableText::Name)
        {
            AddRenderableTextComponent(file, domainName, entity, layerName, i);
        }
        else if (componentType == Attack::Type)
        {
            AddAttackComponent(file, domainName, entity, i);
        }
        else if (componentType == Health::Type)
        {
            AddHealthComponent(file, domainName, entity, i);
        }
        else if (componentType == WarriorLogic::Type)
        {
            AddWarriorLogicComponent(file, domainName, entity, i);
        }
        else if (componentType == BombLogic::Type)
        {
            AddBombLogicComponent(file, domainName, entity, i);
        }
        else if (componentType == BalloonLogic::Type)
        {
            AddBalloonLogicComponent(file, domainName, entity, i);
        }
        else if (componentType == HealthBarLogic::Type)
        {
            AddHealthBarLogicComponent(file, domainName, entity, i);
        }
        else if (componentType == LocationNameLogic::Type)
        {
            AddLocationNameLogicComponent(file, domainName, entity, i);
        }
        else if (componentType == LogoLogic::Type)
        {
            AddLogoLogicComponent(file, domainName, entity, i);
        }
        else
        {
            INVALID_ARGUMENT_EXCEPTION(string::Format("Unwkhown component type: \"{0}\"", componentType));
        }
    }

    return entity;
}

//===========================================================================//

SharedPtr<Entity> EntityManagerSingleton::ReturnCachedEntity(const string & groupName, const string & entityType,
        const string & layerName, const Vector3 & position)
{
    SharedPtr<Entity> entity = _cachedEntities[groupName][entityType].GetFirst();
    entity->ReturnFromCache();
    if (entity->ContainsComponent(AbstractRenderable::Type))
    {
        SharedPtr<AbstractRenderable> renderable = entity->GetComponent<AbstractRenderable>(AbstractRenderable::Type);
        renderable->SetLayer(layerName);
    }
    if (entity->ContainsComponent(SceneNode::Type))
    {
        SharedPtr<SceneNode> sceneNode = entity->GetComponent<SceneNode>(SceneNode::Type);
        sceneNode->MakeIdentity();
        sceneNode->SetPosition(position);
    }
    return entity;
}

//===========================================================================//

void EntityManagerSingleton::AddSceneNodeComponent(const string & domainName, const SharedPtr<Entity> & entity,
        const Vector3 & position)
{
    SharedPtr<SceneNode> sceneNode = new SceneNode(domainName);
    sceneNode->SetPosition(position);
    entity->AddComponent(sceneNode);
}

//===========================================================================//

void EntityManagerSingleton::AddSpriteComponent(const WeakPtr<ParametersFile> & file, const string & domainName,
        const SharedPtr<Entity> & entity, const string & layerName, integer index)
{
    string pathToSprite = file->GetString(string::Format(PathToComponents + ".{0}." + PathToSpriteKey, index));
    SharedPtr<Sprite> sprite = new Sprite(domainName, layerName, pathToSprite);
    sprite->SetSceneNode(entity->GetComponent<SceneNode>(SceneNode::Type));
    entity->AddComponent(sprite);
}

//===========================================================================//

void EntityManagerSingleton::AddAnimatedSpriteComponent(const WeakPtr<ParametersFile> & file,
        const string & domainName, const SharedPtr<Entity> & entity, const string & layerName, integer index)
{
    string pathToSprite = file->GetString(string::Format(PathToComponents + ".{0}." + PathToSpriteKey, index));
    SharedPtr<AnimatedSprite> animatedSprite = new AnimatedSprite(domainName, layerName, pathToSprite);
    animatedSprite->SetSceneNode(entity->GetComponent<SceneNode>(SceneNode::Type));
    entity->AddComponent(animatedSprite);

    integer animationControllerUpdatingOrder = UpdatingOrderTable->GetOrder(AnimationController::Type);
    SharedPtr<AnimationController> animationController = new AnimationController(domainName,
            animationControllerUpdatingOrder, animatedSprite);
    entity->AddComponent(animationController);
}

//===========================================================================//

void EntityManagerSingleton::AddRenderableTextComponent(const WeakPtr<ParametersFile> & file, const string & domainName,
        const SharedPtr<Entity> & entity, const string & layerName, integer index)
{
    string pathToRenderableText = file->GetString(string::Format(PathToComponents + ".{0}." + PathToRenderableTextKey, index));
    SharedPtr<RenderableText> renderableText = new RenderableText(domainName, layerName, pathToRenderableText);
    renderableText->SetSceneNode(entity->GetComponent<SceneNode>(SceneNode::Type));
    entity->AddComponent(renderableText);
}

//===========================================================================//

void EntityManagerSingleton::AddAttackComponent(const WeakPtr<ParametersFile> & file, const string & domainName,
        const SharedPtr<Entity> & entity, integer index)
{
    Interval power = file->GetCustom<Interval>(string::Format(PathToComponents + ".{0}." + PowerKey, index));
    float radius = file->GetFloat(string::Format(PathToComponents + ".{0}." + RadiusKey, index));
    float prepareTime = file->GetFloat(string::Format(PathToComponents + ".{0}." + PrepareTimeKey, index));
    SharedPtr<Attack> attack = new Attack(domainName, power, radius, prepareTime);
    entity->AddComponent(attack);
}

//===========================================================================//

void EntityManagerSingleton::AddHealthComponent(const WeakPtr<ParametersFile> & file, const string & domainName,
        const SharedPtr<Entity> & entity, integer index)
{
    integer maxHealth = file->GetInteger(string::Format(PathToComponents + ".{0}." + MaxHealthKey, index));
    SharedPtr<Health> healthComponent = new Health(domainName, maxHealth);
    entity->AddComponent(healthComponent);

    AddHealthBar(file, domainName, entity, index);
}

//===========================================================================//

void EntityManagerSingleton::AddWarriorLogicComponent(const WeakPtr<ParametersFile> & file,
        const string & domainName, const SharedPtr<Entity> & entity, integer index)
{
    float speed = file->GetFloat(string::Format(PathToComponents + ".{0}." + SpeedKey, index));
    SharedPtr<SceneNode> sceneNode = entity->GetComponent<SceneNode>(SceneNode::Type);
    integer movementUpdatingOrder = UpdatingOrderTable->GetOrder(MovementToPointController::Type);
    SharedPtr<MovementToPointController> movementController = new MovementToPointController(domainName,
            movementUpdatingOrder, sceneNode, Vector3(), speed, Gameplay::MovementAccuracy, false);
    entity->AddComponent(movementController);

    SharedPtr<Flipper> flipper = new Flipper(domainName);
    entity->AddComponent(flipper);

    integer actionListUpdatingOrder = UpdatingOrderTable->GetOrder(WarriorLogic::Type);
    SharedPtr<ActionList> actionList = new ActionList(domainName, actionListUpdatingOrder);
    entity->AddComponent(actionList);

    SharedPtr<WarriorLogic> warriorLogic = new WarriorLogic(domainName);
    entity->AddComponent(warriorLogic);
}

//===========================================================================//

void EntityManagerSingleton::AddBombLogicComponent(const WeakPtr<ParametersFile> & file,
        const string & domainName, const SharedPtr<Entity> & entity, integer index)
{
    Interval power = file->GetCustom<Interval>(string::Format(PathToComponents + ".{0}." + PowerKey, index));
    float radius = file->GetFloat(string::Format(PathToComponents + ".{0}." + RadiusKey, index));
    float fallingSpeed = file->GetFloat(string::Format(PathToComponents + ".{0}." + FallingSpeedKey, index));
    SharedPtr<BombLogic> bombLogic = new BombLogic(domainName, power, radius, fallingSpeed);
    entity->AddComponent(bombLogic);
}

//===========================================================================//

void EntityManagerSingleton::AddBalloonLogicComponent(const WeakPtr<ParametersFile> & file,
        const string & domainName, const SharedPtr<Entity> & entity, integer index)
{
    float speed = file->GetFloat(string::Format(PathToComponents + ".{0}." + SpeedKey, index));
    SharedPtr<BalloonLogic> balloonLogic = new BalloonLogic(domainName, speed);
    entity->AddComponent(balloonLogic);

    SharedPtr<BalloonInputReceiver> balloonInputReceiver = new BalloonInputReceiver(domainName);
    entity->AddComponent(balloonInputReceiver);
}

//===========================================================================//

void EntityManagerSingleton::AddHealthBarLogicComponent(const WeakPtr<ParametersFile> & file,
        const string & domainName, const SharedPtr<Entity> & entity, integer index)
{
    SharedPtr<AbstractSprite> sprite = entity->GetComponent<AbstractSprite>(AbstractRenderable::Type);

    float yellowZonePercent = file->GetFloat(string::Format(PathToComponents + ".{0}." + YellowZonePercentKey, index));
    float redZonePercent = file->GetFloat(string::Format(PathToComponents + ".{0}." + RedZonePercentKey, index));
    Color greenZoneColor = file->GetCustom<Color>(string::Format(PathToComponents + ".{0}." + GreenZoneColorKey, index));
    Color yellowZoneColor = file->GetCustom<Color>(string::Format(PathToComponents + ".{0}." + YellowZoneColorKey, index));
    Color redZoneColor = file->GetCustom<Color>(string::Format(PathToComponents + ".{0}." + RedZoneColorKey, index));
    SharedPtr<HealthBarLogic> healthBarLogic = new HealthBarLogic(domainName, yellowZonePercent, redZonePercent,
            greenZoneColor, yellowZoneColor, redZoneColor, sprite->GetWidth());
    entity->AddComponent(healthBarLogic);
}

//===========================================================================//

void EntityManagerSingleton::AddLocationNameLogicComponent(const WeakPtr<ParametersFile> & file,
        const string & domainName, const SharedPtr<Entity> & entity, integer index)
{
    SharedPtr<AbstractRenderable> renderable = entity->GetComponent<AbstractRenderable>(AbstractRenderable::Type);
    float alphaEnhanceSpeed = file->GetFloat(string::Format(PathToComponents + ".{0}." + AlphaEnhanceSpeedKey, index));
    integer colorAlphaControllerUpdatingOrder = UpdatingOrderTable->GetOrder(ColorAlphaController::Type);
    SharedPtr<ColorAlphaController> colorAlphaController = new ColorAlphaController(domainName,
            colorAlphaControllerUpdatingOrder, renderable, alphaEnhanceSpeed);
    entity->AddComponent(colorAlphaController);

    float _showingTime = file->GetFloat(string::Format(PathToComponents + ".{0}." + ShowingTimeKey, index));
    SharedPtr<LocationNameLogic> locationNameLogic = new LocationNameLogic(domainName, _showingTime);
    entity->AddComponent(locationNameLogic);
}

//===========================================================================//

void EntityManagerSingleton::AddLogoLogicComponent(const WeakPtr<ParametersFile> & file,
        const string & domainName, const SharedPtr<Entity> & entity, integer index)
{
    SharedPtr<AbstractSprite> sprite = entity->GetComponent<AbstractSprite>(AbstractRenderable::Type);
    float alphaEnhanceSpeed = file->GetFloat(string::Format(PathToComponents + ".{0}." + AlphaEnhanceSpeedKey, index));
    integer colorAlphaControllerUpdatingOrder = UpdatingOrderTable->GetOrder(ColorAlphaController::Type);
    SharedPtr<ColorAlphaController> colorAlphaController = new ColorAlphaController(domainName,
            colorAlphaControllerUpdatingOrder, sprite, alphaEnhanceSpeed);
    entity->AddComponent(colorAlphaController);

    float deltaWidth = file->GetFloat(string::Format(PathToComponents + ".{0}." + DeltaWidthKey, index));
    float deltaHeight = file->GetFloat(string::Format(PathToComponents + ".{0}." + DeltaHeightKey, index));
    integer spriteSizeControllerUpdatingOrder = UpdatingOrderTable->GetOrder(SpriteSizeController::Type);
    SharedPtr<SpriteSizeController> spriteSizeController = new SpriteSizeController(domainName,
            spriteSizeControllerUpdatingOrder, sprite, deltaWidth, deltaHeight);
    entity->AddComponent(spriteSizeController);

    float _showingTime = file->GetFloat(string::Format(PathToComponents + ".{0}." + ShowingTimeKey, index));
    SharedPtr<LogoLogic> logoLogic = new LogoLogic(domainName, _showingTime);
    entity->AddComponent(logoLogic);
}

//===========================================================================//

void EntityManagerSingleton::AddHealthBar(const WeakPtr<ParametersFile> & file, const string & domainName,
        const SharedPtr<Entity> & entity, integer index)
{
    SharedPtr<AbstractSprite> sprite = entity->GetComponent<AbstractSprite>(AbstractRenderable::Type);

    float healthBarY = file->GetFloat(string::Format(PathToComponents + ".{0}." + HealthBarYKey, index));
    healthBarY += sprite->GetOrigin().Y;
    Vector3 position(0.0f, healthBarY);

    SharedPtr<Entity> healthBarBackground = CreateNewEntity(domainName, Gameplay::HUDGroupName,
                                            PathToHealthBarBackgroundEntity, Gameplay::HUDLayerName, position);

    SharedPtr<Entity> healthBar = CreateNewEntity(domainName, Gameplay::HUDGroupName, PathToHealthBarEntity,
                                  Gameplay::HUDLayerName, position);

    entity->AddChild(healthBarBackground);
    entity->AddChild(healthBar);

    SharedPtr<SceneNode> entityNode = entity->GetComponent<SceneNode>(SceneNode::Type);
    SharedPtr<SceneNode> healthBarBackgroundNode = healthBarBackground->GetComponent<SceneNode>(SceneNode::Type);
    SharedPtr<SceneNode> healthBarNode = healthBar->GetComponent<SceneNode>(SceneNode::Type);

    entityNode->AddChild(healthBarBackgroundNode);
    entityNode->AddChild(healthBarNode);
}

//===========================================================================//

void EntityManagerSingleton::AddWithChildren(const SharedPtr<Entity> & entity)
{
    for (const SharedPtr<Entity> & child : entity->GetChildren())
    {
        AddWithChildren(child);
    }

    _entities[entity->GetGroupName()].Add(entity);
}

//===========================================================================//

void EntityManagerSingleton::RemoveWithChildren(const SharedPtr<Entity> & entity)
{
    for (const SharedPtr<Entity> & child : entity->GetChildren())
    {
        RemoveWithChildren(child);
    }

    _entities[entity->GetGroupName()].Remove(entity);
}

//===========================================================================//

void EntityManagerSingleton::AddWithChildrenToCache(const SharedPtr<Entity> & entity)
{
    for (const SharedPtr<Entity> & child : entity->GetChildren())
    {
        AddWithChildrenToCache(child);
    }

    const string & groupName = entity->GetGroupName();
    const string & type = entity->GetType();

    if (!_cachedEntities[groupName].Contains(type))
    {
        _cachedEntities[groupName].Add(type, List<SharedPtr<Entity>>());
    }

    _cachedEntities[groupName][type].Add(entity);
}

//===========================================================================//

void EntityManagerSingleton::RemoveWithChildrenFromCache(const SharedPtr<Entity> & entity)
{
    for (const SharedPtr<Entity> & child : entity->GetChildren())
    {
        RemoveWithChildrenFromCache(child);
    }

    _cachedEntities[entity->GetGroupName()][entity->GetType()].Remove(entity);
}

//===========================================================================//

void EntityManagerSingleton::PrintEntities() const
{
    Console->Notice("Entities:");
    Console->AddOffset(2);
    for (auto & entityPair : _entities)
    {
        Console->Notice(string::Format("{0}:", entityPair.GetKey()));
        Console->AddOffset(2);
        for (auto & entity : entityPair.GetValue())
        {
            Console->Notice(string::Format("- {0}", entity->GetName()));
        }
        Console->ReduceOffset(2);
    }
    Console->ReduceOffset(2);

    Console->Notice("Cached entities:");
    Console->AddOffset(2);
    for (auto & groupPair : _cachedEntities)
    {
        Console->Notice(string::Format("{0}:", groupPair.GetKey()));
        Console->AddOffset(2);
        for (auto & entityPair : groupPair.GetValue())
        {
            for (auto & entity : entityPair.GetValue())
            {
                Console->Notice(string::Format("- {0}", entity->GetName()));
            }
        }
        Console->ReduceOffset(2);

    }
    Console->ReduceOffset(2);
}

//===========================================================================//

void EntityManagerSingleton::RegisterCommands()
{
    ScriptSystem->RegisterCommand(new ClassConstCommand<EntityManagerSingleton, void>("Entities", this, &EntityManagerSingleton::PrintEntities));
}

//===========================================================================//

void EntityManagerSingleton::UnregisterCommands()
{
    ScriptSystem->UnregisterCommand("Entities");
}

//===========================================================================//

} // namespace Citadel
