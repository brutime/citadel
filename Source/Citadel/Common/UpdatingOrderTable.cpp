﻿//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "UpdatingOrderTable.h"

#include "../Intro/WidgetsComponents/LogoLogic.h"
#include "../Intro/IntroLogic.h"
#include "../Tutorial/TutorialLogic.h"
#include "../Gameplay/CommonComponents/Flipper.h"
#include "../Gameplay/HUDComponents/HealthBarLogic.h"
#include "../Gameplay/HUDComponents/LocationNameLogic.h"
#include "../Gameplay/Menus/LocationCompleteMenuLogic.h"
#include "../Gameplay/Logic/Bomber.h"
#include "../Gameplay/Logic/Undertaker.h"
#include "../Gameplay/Logic/EndLocationChecker.h"
#include "../Gameplay/Logic/EnemyGenerator/Respawn.h"
#include "../Gameplay/Units/Balloon/BalloonLogic.h"
#include "../Gameplay/Units/Warrior/WarriorLogic.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

SharedPtr<UpdatingOrderTableSingleton> UpdatingOrderTable;

//===========================================================================//

const string UpdatingOrderTableSingleton::Name = "Updating order table";

//===========================================================================//

UpdatingOrderTableSingleton::UpdatingOrderTableSingleton():
    _updatingOrders()
{
    Console->Notice(string::Format("{0} initializing...", Name));
    Console->AddOffset(2);

    FillUpdatingOrders();

    Console->ReduceOffset(2);
    Console->Notice(string::Format("{0} initialized", Name));
}

//===========================================================================//

UpdatingOrderTableSingleton::~UpdatingOrderTableSingleton()
{
    Console->Notice(string::Format("{0} shutting down...", Name));
    Console->Notice(string::Format("{0} has shut down", Name));
}

//===========================================================================//

integer UpdatingOrderTableSingleton::GetOrder(const string & type)
{
    return _updatingOrders[type];
}

//===========================================================================//

void UpdatingOrderTableSingleton::FillUpdatingOrders()
{
    _updatingOrders.Add(LogoLogic::Type, 0);
    _updatingOrders.Add(IntroLogic::Type, 1);

    _updatingOrders.Add(TutorialLogic::Type, 0);

    _updatingOrders.Add(BalloonLogic::Type, 0);
    _updatingOrders.Add(Respawn::Type, 1);
    _updatingOrders.Add(WarriorLogic::Type, 1);
    _updatingOrders.Add(LocationNameLogic::Type, 1);
    _updatingOrders.Add(LocationCompleteMenuLogic::Type, 1);
    _updatingOrders.Add(MovementToPointController::Type, 2);
    _updatingOrders.Add(HealthBarLogic::Type, 3);
    _updatingOrders.Add(AnimationController::Type, 4);
    _updatingOrders.Add(ColorAlphaController::Type, 4);
    _updatingOrders.Add(SpriteSizeController::Type, 4);
    _updatingOrders.Add(Flipper::Type, 5);
    _updatingOrders.Add(Bomber::Type, 6);
    _updatingOrders.Add(Undertaker::Type, 7);
    _updatingOrders.Add(EndLocationChecker::Type, 8);
}

//===========================================================================//

} // namespace Citadel
