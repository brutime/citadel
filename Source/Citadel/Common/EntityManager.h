//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef ENTITY_MANAGER_H
#define ENTITY_MANAGER_H

#include "../../BruTech/Source/Helpers/Engine/Engine.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class EntityManagerSingleton
{
public:
    EntityManagerSingleton();
    ~EntityManagerSingleton();

    SharedPtr<Entity> CreateEntity(const string & domainName, const string & groupName,
                                   const string & pathToEntity, const string & layerName,
                                   const Vector3 & position);

    void ReserveEntities(const string & domainName, const string & groupName,
                         const string & pathToEntity, const string & layerName,
                         const Vector3 & position, integer count);

    bool Contains(const SharedPtr<Entity> & entity);
    void Remove(const SharedPtr<Entity> & entity);

    void AddGroup(const string & groupName);
    const List<SharedPtr<Entity>> & GetGroup(const string & groupName) const;
    bool IsGroupEmpty(const string & groupName);
    void ClearGroup(const string & groupName);
    void RemoveGroup(const string & groupName);

    void Clear();

    integer GetCount(const string & groupName) const;
    integer GetCount() const;

    void PrintEntities() const;

private:
    static const string Name;

    static const string TypeKey;
    static const string PathToComponents;
    static const string ComponentTypeKey;
    static const string PathToSpriteKey;
    static const string PathToRenderableTextKey;
    static const string SpeedKey;
    static const string InertiaSlowdownFactorKey;
    static const string PowerKey;
    static const string RadiusKey;
    static const string PrepareTimeKey;
    static const string FallingSpeedKey;
    static const string MaxHealthKey;
    static const string HealthBarYKey;
    static const string YellowZonePercentKey;
    static const string RedZonePercentKey;
    static const string GreenZoneColorKey;
    static const string YellowZoneColorKey;
    static const string RedZoneColorKey;
    static const string AlphaEnhanceSpeedKey;
    static const string ShowingTimeKey;
    static const string DeltaWidthKey;
    static const string DeltaHeightKey;

    //Возможно это нужно вынести в настройки
    static const string PathToHealthBarBackgroundEntity;
    static const string PathToHealthBarEntity;

    SharedPtr<Entity> CreateNewEntity(const string & domainName, const string & groupName,
                                      const string & pathToEntity, const string & layerName,
                                      const Vector3 & position);

    SharedPtr<Entity> ReturnCachedEntity(const string & groupName, const string & entityType,
                                         const string & layerName, const Vector3 & position);

    void AddSceneNodeComponent(const string & domainName, const SharedPtr<Entity> & entity,
                               const Vector3 & position);

    void AddSpriteComponent(const WeakPtr<ParametersFile> & file, const string & domainName,
                            const SharedPtr<Entity> & entity, const string & layerName, integer index);

    void AddAnimatedSpriteComponent(const WeakPtr<ParametersFile> & file, const string & domainName,
                                    const SharedPtr<Entity> & entity, const string & layerName, integer index);

    void AddRenderableTextComponent(const WeakPtr<ParametersFile> & file, const string & domainName,
                                    const SharedPtr<Entity> & entity, const string & layerName, integer index);

    void AddAttackComponent(const WeakPtr<ParametersFile> & file, const string & domainName,
                            const SharedPtr<Entity> & entity, integer index);

    void AddHealthComponent(const WeakPtr<ParametersFile> & file, const string & domainName,
                            const SharedPtr<Entity> & entity, integer index);

    void AddWarriorLogicComponent(const WeakPtr<ParametersFile> & file, const string & domainName,
                                  const SharedPtr<Entity> & entity, integer index);

    void AddBombLogicComponent(const WeakPtr<ParametersFile> & file, const string & domainName,
                               const SharedPtr<Entity> & entity, integer index);

    void AddBalloonLogicComponent(const WeakPtr<ParametersFile> & file, const string & domainName,
                                  const SharedPtr<Entity> & entity, integer index);

    void AddHealthBarLogicComponent(const WeakPtr<ParametersFile> & file, const string & domainName,
                                    const SharedPtr<Entity> & entity, integer index);

    void AddLocationNameLogicComponent(const WeakPtr<ParametersFile> & file, const string & domainName,
                                       const SharedPtr<Entity> & entity, integer index);

    void AddLogoLogicComponent(const WeakPtr<ParametersFile> & file, const string & domainName,
                               const SharedPtr<Entity> & entity, integer index);

    void AddHealthBar(const WeakPtr<ParametersFile> & file, const string & domainName,
                      const SharedPtr<Entity> & entity, integer index);

    void AddWithChildren(const SharedPtr<Entity> & entity);
    void RemoveWithChildren(const SharedPtr<Entity> & entity);

    void AddWithChildrenToCache(const SharedPtr<Entity> & entity);
    void RemoveWithChildrenFromCache(const SharedPtr<Entity> & entity);

    void RegisterCommands();
    void UnregisterCommands();

    //имя группы, список сущностей
    TreeMap<string, List<SharedPtr<Entity>>> _entities;
    //имя группы, имя типа, список сущностей
    TreeMap<string, TreeMap<string, List<SharedPtr<Entity>>>> _cachedEntities;

    EntityManagerSingleton(const EntityManagerSingleton &) = delete;
    EntityManagerSingleton & operator =(const EntityManagerSingleton &) = delete;
};

//===========================================================================//

extern SharedPtr<EntityManagerSingleton> EntityManager;

//===========================================================================//

} // namespace Citadel

#endif // ENTITY_MANAGER_H
