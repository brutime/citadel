﻿//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Таблица порядка обновления
//

#pragma once
#ifndef UPDATING_ORDER_TABLE_H
#define UPDATING_ORDER_TABLE_H

#include "../../BruTech/Source/Helpers/Engine/Engine.h"

using namespace Bru;

namespace Citadel
{

//===========================================================================//

class UpdatingOrderTableSingleton
{
public:
    static const string Name;

    UpdatingOrderTableSingleton();
    ~UpdatingOrderTableSingleton();

    integer GetOrder(const string & type);

private:
    void FillUpdatingOrders();

    TreeMap<string, integer> _updatingOrders;

    UpdatingOrderTableSingleton(const UpdatingOrderTableSingleton &) = delete;
    UpdatingOrderTableSingleton & operator =(const UpdatingOrderTableSingleton &) = delete;
};

//===========================================================================//

extern SharedPtr<UpdatingOrderTableSingleton> UpdatingOrderTable;

//===========================================================================//
} // namespace Citadel

#endif // UPDATING_ORDER_TABLE_H
